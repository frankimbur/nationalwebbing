<?php
########################################################################
# Copyright 1999 Desktop Solutions Software, Inc.
# 631-493-3422
# info@desktopsolutions.com
# www.desktopsolutions.com
# usage rights granted for use on a per-project-basis
#########################################################################
//  Uncomment for session management, and end with page_close();
require ("prepend.php3");
#$DSDEBUG=1;
page_open(
array("sess" => "DS_Session",
"auth" => "DS_Auth",
"perm" => "DS_Perm"));
if (!HasPermissions("admin"))
    return;
#########################################################################
#########################################################################
# Add/Edit/Delete routine for shogyo.sitesetup
# generated at Monday 09th of April 2001 11:59:23 AM
##############
//* Params
//* key - value of key, denotes add or edit
//* delete = 1
//* if neither are present, script will assume add mode
$form = new DSForm;
## Optional settings:
#$form->AddAnother=1;

#THERE IS ONLY EVER ONE RECORD IN THIS TABLE
$key=1;
if (!OneSQLValue("SELECT 1 from ziplocateoption where ziplocateoption_no=1")) {
    ## First run, generate a blank record, let defaults fill it..
    DoQuery("INSERT INTO ziplocateoption (ziplocateoption_no) VALUES (1)");
}

$CurrentRecord = ReadCurrentRecord("ziplocateoption","ziplocateoption_no",$key);

if (isset($do_over)) {
    // we've been called from ourselves, second time in a row...
    // the key should have been passed in, causing defaults to be loaded above
    // now when we unset , the rest of the script will be in 'add' mode..
    unset($key);
}
//Set up fields here
if (isset($key)) {
    $form->add_element(array(
                        "type"=>"hidden",
                        "name"=>"key",
                        "value"=>$key
                        ));
}
if (isset($delete)) {
    $form->add_element(array(
                        "type"=>"hidden",
                        "name"=>"delete",
                        "value"=>1
                        ));
}
if (isset($returnto)) {
    $form->add_element(array(
                        "type"=>"hidden",
                        "name"=>"returnto",
                        "value"=>$returnto
                        ));
}

$permquery = "SELECT template,template_no FROM template WHERE template_no<>999 ORDER BY 1";
$sqlarray = FillSQLArrayTwo($permquery);

$form->add_element(array(
"type"=>"select",
"name"=>"field_module_template_no",
"value"=>$CurrentRecord->module_template_no,
"options"=>$sqlarray));

$form->add_element(array(
"type"=>"text",
"name"=>"field_systemname",
"value"=>$CurrentRecord->systemname,
"size"=>36,
"maxlength"=>40,
"minlength"=>1,
"length_e"=>"Please enter the system name field."
));

$form->add_element(array(
"type"=>"text",
"name"=>"field_default_miles",
"value"=>$CurrentRecord->default_miles,
"size"=>3,
"maxlength"=>3,
"minlength"=>1,
"length_e"=>"Please enter the default miles field."
));


$form->add_element(array(
"type"=>"textarea",
"name"=>"field_html_locate_page",
"value"=>$CurrentRecord->html_locate_page,
"size"=>0,
"rows"=>6,
"cols"=>60
));

$form->add_element(array(
"type"=>"textarea",
"name"=>"field_html_result_page",
"value"=>$CurrentRecord->html_result_page,
"size"=>0,
"rows"=>6,
"cols"=>60
));

$form->add_element(array(
"type"=>"select",
"name"=>"field_display_directions",
"value"=>$CurrentRecord->display_directions,
"size"=>1,
"options"=>YNSelectArray()
,"maxlength"=>1,
"minlength"=>1,
"length_e"=>"Please enter the Display Directions."
));

$form->add_element(array(
"type"=>"select",
"name"=>"field_display_distance",
"value"=>$CurrentRecord->display_distance,
"size"=>1,
"options"=>YNSelectArray()
,"maxlength"=>1,
"minlength"=>1,
"length_e"=>"Please enter the Display Distance."
));



//if being called from myself, do validation..
if ($Submit) {

    if ($err=$form->validate()) {
        echo $err;
    }
    else {
        // send to SQL
        if (isset($key)) {
            // UPDATE
            if ( ! isset($delete)) {
               $retval=DoUpdate($HTTP_POST_VARS,"ziplocateoption","ziplocateoption_no",$key);
            }
            else {
            // DELETE, CHECK FOR RELATED RECORDS IF REQUIRED

               $retval=DoDelete("ziplocateoption","ziplocateoption_no",$key);
            }
        }
        else {
            $retval=DoInsert($HTTP_POST_VARS,"ziplocateoption");
            if (isset($AddAnother)) {
                // was this a table with an SAPK?
                $key=mysql_insert_id();
                if (!$key) {
                    // no - there is a UAPK..
                    $key = $field_ziplocateoption_no;
                }
                // send the user back to this script, cause a do-over...
                Header("Location: sitesetup_edit.dtop?key=$key&do_over=1");
                exit();
            }
        }
        if ($retval)
            $form->SaveAndReturn();
        return;
    }
}


//Begin HTML here
$systemname = OneSQLValue( "select systemname from ziplocateoption where ziplocateoption_no = 1" );
$module_template_no = 999;  // use admin template
DSBeginPage("$systemname Options",0,1,$module_template_no);

$form->StartForm("$systemname Options");
if (isset( $delete ) || isset($view) )  {
    $form->freeze();
}
if (isset($delete))
    echo "Press SUBMIT to confirm deletion.";

$form->DrawField("field_systemname","System Name",1,1,0);
$form->DrawField("field_module_template_no","Template",1,1,0);
$form->DrawField("field_default_miles","Default Miles",1,1,0);
$form->DrawField("field_display_directions","Display directions link ?",1,1,0);
$form->DrawField("field_display_distance","Display distance ?",1,1,0);
$form->DrawField("field_html_locate_page","Text for locate page:".DisplayHTMLEditButton("field_html_locate_page"),1,0,0);
$form->DrawField("field_html_result_page","Text for result page:".DisplayHTMLEditButton("field_html_result_page"),1,0,0);
$form->EndForm(!isset($key));
PreviousMenu("newsmenu.php");


// Save data back to database.
DSEndPage(1,0,$module_template_no);
?>
