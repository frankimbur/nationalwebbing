<?php

/*
 * lucid calendar :: web based calendaring software. Version 0.2
 * Copyright (C) 1999 Chris Lea <chrislea@luciddesigns.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*
 * define some global variables to be used in various places
 */
$SECONDS_PER_DAY = 60*60*24;


// returns 1 is $value is in $array, 0 otherwise
function is_in_array( $value , $array )
{
  for ( $i=0 ; $i < sizeof($array) ; $i++ ){
    if( !strcmp( $value , $array[$i] ) ){
      return 1;
    }
  }
  return 0;
}

// trim leading and trailing whitespace
function cl_trim ($s) {
  $ret = eregi_replace ("^[[:space:]]+|[[:space:]]+$","",$s);
  return $ret;
}

/*
 * function to generate the javascript needed for the popup
 */
function js_popup($w=300,$h=300)
{
  echo "<script language=\"javascript\">\n";
  echo "<!--\n\n";
  echo "function openWin(id){\n\n";
  echo "  w = window.open('showevent.html?id='+id,'event'+id,'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=$w,height=$h');\n\n";
  echo "}\n\n";
  echo "// -->\n";
  echo "</script>\n\n";
}

/*
 * function to get event title and text
 */
function get_event_info($id) {

  $query = "SELECT msg_title,msg_text FROM calendar_messages WHERE msg_id='$id'";
  $result = DoQuery($query);
  if( !$result ){
    echo mysql_error() . ": " . mysql_errno();
  }

  $message_hash = mysql_fetch_array($result);

  mysql_free_result($result);
  return $message_hash;
}


/*************************************************/

class month {

  var $months_hash = array( "01" => "January",
                            "02" => "February",
                            "03" => "March",
                            "04" => "April",
                            "05" => "May",
                            "06" => "June",
                            "07" => "July",
                            "08" => "August",
                            "09" => "September",
                            "10" => "October",
                            "11" => "November",
                            "12" => "December" );

  var $month_name;
  var $month_number;
  var $year;
  var $month_data;
  var $nextmonth;
  var $nextyear;
  var $prevmonth;
  var $prevyear;

///////////////////////////////////////////
// constructor function
///////////////////////////////////////////
  function month( $thismonth = "" , $thisyear = "" ){
    if( !$thismonth ){
      $thismonth = date("m");
    }

    if( !$thisyear ){
      $thisyear = date("Y");
    }

    $this->month_name = $this->months_hash[$thismonth];
    $this->month_number = $thismonth;
    $this->year = $thisyear;

    $this->nextmonth = sprintf("%02d",$this->month_number+1);
    $this->prevmonth = sprintf("%02d",$this->month_number-1);
    $this->nextyear = $this->prevyear = $thisyear;

    if( $this->month_number == "12" ){
      $this->nextmonth = "01";
      $this->nextyear = $thisyear + 1;
    }
    if( $this->month_number == "01" ){
      $this->prevmonth = "12";
      $this->prevyear = $thisyear - 1;
    }

    /*
     * month data
     */
    $query = "SELECT msg_id,msg_day,msg_title,msg_time,msg_location,char_length(msg_text) as len FROM calendar_messages WHERE
           msg_month='" . $this->month_number . "' AND
           msg_year='" . $this->year ."' AND
           active='Y' AND closed='N'
           ORDER BY msg_day";
    $result = DoQuery($query);
    if( !$result ){
      echo mysql_error() . ": " . mysql_errno();
    }

    while ($tmp = mysql_fetch_array($result)) {
      if( strcmp( $tmp["msg_title"] , "" ) != 0 ){
        $this->month_data[$tmp["msg_day"]]["id"][] = $tmp["msg_id"];
        $this->month_data[$tmp["msg_day"]]["event_title"][] = $tmp["msg_title"];
        $this->month_data[$tmp["msg_day"]]["event_time"][] = $tmp["msg_time"];
        $this->month_data[$tmp["msg_day"]]["event_public"][] = $tmp["msg_public"];
        $this->month_data[$tmp["msg_day"]]["event_location"][] = $tmp["msg_location"];
        $this->month_data[$tmp["msg_day"]]["len"][] = $tmp["len"];
      }
    }
  }



  // obvious function
  function print_month_name() {
    echo $this->month_name;
  }

  // obvious function
  function print_year() {
    echo $this->year;
  }

  // obvious function
  function print_datestring() {
    echo $this->month_name . ", " . $this->year;
  }


  ///////////////////////////////////////////
  // returns the number of days for a given
  // month and year. Months go 1-12 and
  // years are numeric such as "1999"
  ///////////////////////////////////////////
  function days_in_month( $month, $year ){
    // older versions of php don't support "t" in the date() function,
    // so I have to do this really kludgy thing.
    if( $month == "01" ){
      $days_in_month = 31;
    }
    // have to handle leap year
    if( $month == "02" && $year % 4 == 0 ){
      $days_in_month = 29;
    }
    else if( $month == "02" ){
      $days_in_month = 28;
    }

    if( $month == "03" ){
      $days_in_month = 31;
    }
    if( $month == "04" ){
      $days_in_month = 30;
    }
    if( $month == "05" ){
      $days_in_month = 31;
    }
    if( $month == "06" ){
      $days_in_month = 30;
    }
    if( $month == "07" ){
      $days_in_month = 31;
    }
    if( $month == "08" ){
      $days_in_month = 31;
    }
    if( $month == "09" ){
      $days_in_month = 30;
    }
    if( $month == "10" ){
      $days_in_month = 31;
    }
    if( $month == "11" ){
      $days_in_month = 30;
    }
    if( $month == "12" ){
      $days_in_month = 31;
    }
    return $days_in_month;
  }



  /* this one's for internal use */
  function _get_date_by_counter($i,$month,$year){

    $first_day = date("w" , mktime(0,0,0,$month,1,$year));
    //$days_in_month = date("t" , mktime(0,0,0,$month,1,$year));
    // older versions of php don't support "t" in the date() function,
    // so I have to do this really kludgy thing.
    if( $month == "01" ){
      $days_in_month = 31;
    }
    // have to handle leap year
    if( $month == "02" && $year % 4 == 0 ){
      $days_in_month = 29;
    }
    else if( $month == "02" ){
      $days_in_month = 28;
    }

    if( $month == "03" ){
      $days_in_month = 31;
    }
    if( $month == "04" ){
      $days_in_month = 30;
    }
    if( $month == "05" ){
      $days_in_month = 31;
    }
    if( $month == "06" ){
      $days_in_month = 30;
    }
    if( $month == "07" ){
      $days_in_month = 31;
    }
    if( $month == "08" ){
      $days_in_month = 31;
    }
    if( $month == "09" ){
      $days_in_month = 30;
    }
    if( $month == "10" ){
      $days_in_month = 31;
    }
    if( $month == "11" ){
      $days_in_month = 30;
    }
    if( $month == "12" ){
      $days_in_month = 31;
    }

    if( $i < $first_day ){
      return "&nbsp;";
    }
    if( $i >= $days_in_month+$first_day ){
      return "&nbsp;";
    }

    return ($i+1-$first_day);
  }


  /*
   * this is the big cahoona function,
   * draws a calendar.
   */
  function draw($draw_array = "") {

  /*
   * this is a long section which simply gets
   * the parameters which are used in the drawing
   * of the calendar. It's not pretty, but it's
   * simple to understand and modify so I'm
   * running with it.
   */


    if( !$draw_array["textcolor"] ){
      $textcolor = "#000000";
    }
    else{
      $textcolor = $draw_array["textcolor"];
    }

    if( !$draw_array["bgcolor"] ){
      $bgcolor = "#cccccc";
    }
    else{
      $bgcolor = $draw_array["bgcolor"];
    }

    if( !$draw_array["font_face"] ){
      $font_face = "Verdana, Arial, Helvetica";
    }
    else{
      $font_face = $draw_array["font_face"];
    }

    if( !$draw_array["font_size"] ){
      $font_size = "-1";
    }
    else{
      $font_size = $draw_array["font_size"];
    }

    if( !$draw_array["table_width"] ){
      $table_width = "100";
    }
    else{
      $table_width = $draw_array["table_width"];
    }

    if( !$draw_array["table_height"] ){
      $table_height = "100";
    }
    else{
      $table_height = $draw_array["table_height"];
    }

    if( !$draw_array["cellpadding"] ){
      $cellpadding = "0";
    }
    else{
      $cellpadding = $draw_array["cellpadding"];
    }

    if( !$draw_array["cellspacing"] ){
      $cellspacing = "0";
    }
    else{
      $cellspacing = $draw_array["cellspacing"];
    }

    if( !$draw_array["table_border"] ){
      $table_border = "0";
    }
    else{
      $table_border = $draw_array["table_border"];
    }

    if( !$draw_array["top_row_align"] ){
      $table_top_row_align = "left";
    }
    else{
      $table_top_row_align = $draw_array["top_row_align"];
    }

    if( !$draw_array["top_row_valign"] ){
      $table_top_row_valign = "top";
    }
    else{
      $table_top_row_valign = $draw_array["top_row_valign"];
    }

    if( !$draw_array["row_align"] ){
      $table_row_align = "left";
    }
    else{
      $table_row_align = $draw_array["row_align"];
    }

    if( !$draw_array["row_valign"] ){
      $table_row_valign = "top";
    }
    else{
      $table_row_valign = $draw_array["row_valign"];
    }

    if( !$draw_array["top_row_cell_height"] ){
      $table_top_row_cell_height = "";
    }
    else{
      $table_top_row_cell_height = $draw_array["top_row_cell_height"];
    }

    /*
     * end of "getting drawing parameters section.
     */
    /***************************************************/

    /* adjust if width is specified in pixels */
    if( eregi("px",$table_width) ){
      $table_width = eregi_replace("px" , "" , $table_width);
    }
    else if( $table_width ){
      $table_width = $table_width . "%";
    }

   /*
    * for some reason, it seems that we have to handle height
    * a little bit differently. It should always be in pixels
    */

    $table_height = eregi_replace("[^[:digit:]]" , "" , $table_height);
    if( !ereg("^[[:digit:]]+$" , $table_height ) ){
      $table_height = "250";
    }


    /*
     * we need to know how many rows are going to be in this table
     */


    if( $this->days_in_month($this->month_number,$this->year) == 28 && date("w" , mktime(0,0,0,2,1,$this->year)) == 0 ){
      $num_of_rows = 4;
    }
    else if( $this->days_in_month($this->month_number,$this->year) == 30 && date("w" , mktime(0,0,0,$this->month_number,1,$this->year)) > 5 ){
      $num_of_rows = 6;
    }
    else if( $this->days_in_month($this->month_number,$this->year) == 31 && date("w" , mktime(0,0,0,$this->month_number,1,$this->year)) > 4 ){
      $num_of_rows = 6;
    }
    else{
      $num_of_rows = 5;
    }

    /* start printout of main calendar table */
    echo "<!-- begin calendar printout -->\n";
    echo "<table cellspacing=\"$cellspacing\" cellpadding=\"$cellpadding\" width=\"$table_width\" height=\"$table_height\" border=\"$table_border\">\n";

    /*
     * we need to figure out the cell height and width for each of these.
     */

    $dates_cell_height = ceil(($table_height - $table_top_row_cell_height) / $num_of_rows);

    /* deal with widths given in percentages or in pixels */
    if( ereg( "%" , $table_width ) ){
      $dates_cell_width = sprintf( "%.3f" , eregi_replace("%","",$table_width)/7 ) . "%";
    }
    else{
      $dates_cell_width = ceil( $table_width / 7 );
    }

    /*
     * this prints out the top row, which has the names of the
     * days of the week. I consider it a distinct sort of thing.
     */
    echo "<tr>\n";
    echo "  <td bgcolor=\"$bgcolor\" align=\"$table_top_row_align\" valign=\"$table_top_row_valign\" height=\"$table_top_row_cell_height\" width=\"$dates_cell_width\"><font face=\"$font_face\" size=\"$font_size\"><b>Sunday</b></font></td>\n";
    echo "  <td bgcolor=\"$bgcolor\" align=\"$table_top_row_align\" valign=\"$table_top_row_valign\" height=\"$table_top_row_cell_height\" width=\"$dates_cell_width\"><font face=\"$font_face\" size=\"$font_size\"><b>Monday</b></font></td>\n";
    echo "  <td bgcolor=\"$bgcolor\" align=\"$table_top_row_align\" valign=\"$table_top_row_valign\" height=\"$table_top_row_cell_height\" width=\"$dates_cell_width\"><font face=\"$font_face\" size=\"$font_size\"><b>Tuesday</b></font></td>\n";
    echo "  <td bgcolor=\"$bgcolor\" align=\"$table_top_row_align\" valign=\"$table_top_row_valign\" height=\"$table_top_row_cell_height\" width=\"$dates_cell_width\"><font face=\"$font_face\" size=\"$font_size\"><b>Wednesday</b></font></td>\n";
    echo "  <td bgcolor=\"$bgcolor\" align=\"$table_top_row_align\" valign=\"$table_top_row_valign\" height=\"$table_top_row_cell_height\" width=\"$dates_cell_width\"><font face=\"$font_face\" size=\"$font_size\"><b>Thursday</b></font></td>\n";
    echo "  <td bgcolor=\"$bgcolor\" align=\"$table_top_row_align\" valign=\"$table_top_row_valign\" height=\"$table_top_row_cell_height\" width=\"$dates_cell_width\"><font face=\"$font_face\" size=\"$font_size\"><b>Friday</b></font></td>\n";
    echo "  <td bgcolor=\"$bgcolor\" align=\"$table_top_row_align\" valign=\"$table_top_row_valign\" height=\"$table_top_row_cell_height\" width=\"$dates_cell_width\"><font face=\"$font_face\" size=\"$font_size\"><b>Saturday</b></font></td>\n";
    echo "</tr>\n";


    /*
     * now print out all the cells for the days of the
     * month. This is the "heart" of this function.
     */

    for( $i=0 ; $i < $num_of_rows*7 ; $i++ ){
      /* start first row */
      if( $i==0 ){
        echo "<tr>\n";
      }
      /* break into a new row at the appropriate places */
      if( $i%7 == 0 && $i != 0){
        echo "</tr>\n";
        echo "<tr>\n";
      }

      /*
       * get the current day
       */
      $theday = $this->_get_date_by_counter($i,$this->month_number, $this->year);


      /*
       * if there's an event for this day, get it.
       * otherwise, set to "" string
       */

      if( $this->month_data[$theday]["event_title"][0] ){
        for( $j=0 ; $j <  count($this->month_data[$theday]["event_title"]) ; $j++ ){
         if ($this->month_data[$theday]['event_public'][$j]=='Y') {
                $color="green";
         }
         else {
                $color="black";
         }
         if ($this->month_data[$theday]['len'][$j] > 0) {
                $linkstart="<a href=calendar_message_detail.html?key=".$this->month_data[$theday]['id'][$j].">";
                $linkend="</a";
         }
         else {
                $linkstart="";
                $linkend="";
         }
          $theevent .=
          "$linkstart <font face=\"$font_face\" size=\"$font_size\" color=$color>".
          "<i>".$this->month_data[$theday]['event_title'][$j]."</i><BR>".
          $this->month_data[$theday]['event_time'][$j]." ".
          $this->month_data[$theday]['event_location'][$j]."$linkend </font><br><br>";
        }
      }
      else{
        $theevent = "";
      }
      echo "  <td bgcolor=\"$bgcolor\" align=\"$table_row_align\" valign=\"$table_row_valign\" height=\"$dates_cell_height\" width=\"$dates_cell_width\"><font face=\"$font_face\" size=\"$font_size\">$theday<br>$theevent</font></td>\n";
      /* be sure to clear out $theevent */
      $theevent = "";
      /* close the last row */
      if( $i == $num_of_rows*7-1 ){
        echo "</tr>\n";
      }
    }
    echo "</table>\n";
    echo "<!-- end calendar -->\n"; /* end of calendar printout */
  } /* end draw function */

} /*** end of class "month" ***/

?>
