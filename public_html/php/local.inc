<?php
/*
 * Session Management for PHP3
 *
 * Copyright (c) 1998,1999 SH Online Dienst GmbH
 *                    Boris Erdmann, Kristian Koehntopp
 *
 * $Id: local.inc,v 1.1.1.1 2001/11/28 20:11:54 tom Exp $
 *
 */
#include ("../private/.database.inc");
include ("../private/.database.inc");


class DS_CT_Sql extends CT_Sql {
  var $database_class = "DB_Example";          ## Which database to connect...
  var $database_table = "active_sessions"; ## and find our session data in this table.
}

class DS_Session extends Session {
  var $classname = "DS_Session";

  var $cookiename     = "";                ## defaults to classname
  var $magic          = "Hocuspocus";      ## ID seed
  var $mode           = "cookie";          ## We propagate session IDs with cookies
  var $fallback_mode;
  var $lifetime       = 30000;                 ## 0 = do session cookies, else minutes
  var $that_class     = "DS_CT_Sql"; ## name of data storage container
  var $gc_probability = 5;
}

class DS_User extends User {
  var $classname = "DS_User";

  var $magic          = "Abracadabra";     ## ID seed
  var $that_class     = "DS_CT_Sql"; ## data storage container
}

class DS_Auth extends Auth {
  var $classname      = "DS_Auth";

  var $lifetime       =  30000;

  var $database_class = "DB_Example";
  var $database_table = "auth_user";

  function auth_loginform() {
    global $sess;
    global $_PHPLIB;
    include($_PHPLIB["libdir"] . "loginform.ihtml");
  }

  function auth_validatelogin() {
    global $username, $password,$libpath;

    if(isset($username)) {
      $this->auth["uname"]=$username;        ## This provides access for "loginform.ihtml"
    }


	$uid = false;

    $this->db->query(sprintf("select uid, perms
                                     from %s
                                    where username = '%s'
                                    and password= encode('%s',\"$libpath\")
                                    and active='Y'",
                          $this->database_table,
                          addslashes($username),
                          addslashes($password)));
   while($this->db->next_record()) {
      $uid = $this->db->f("uid");
      $this->auth["perm"] = $this->db->f("perms");
    }

    # TJM 8/27/01 - add to log table
    AddToLog("LOGIN","NONE","NONE");
    #FTI 3/27/00 update lastlogon date and times logged in
    $this->db->query("
        update $this->database_table
        set
        lastlogon=now(),
        timesloggedon = timesloggedon+1
        where
        uid='$uid'");
    # FTI 6/13/00 update user ID in session table
    global $sess;
    DoQuery("UPDATE active_sessions SET uid='$uid' WHERE sid='$sess->id' ");
    # TJM 01/21/2002
#        echo "got here!";
#        return;
    if (defined("CHAT")) {
        global $PHORUM;
        if (strtoupper($this->auth["perm"])=="ADMIN") {
            $admin_user_id = $uid;
            $PHORUM["admin_user"]["id"] = $admin_user_id;

            $qc = "SELECT forum_id FROM forums_moderators WHERE user_id=$uid";
            $rtval = DoQuery($qc);
            while ($rc = mysql_fetch_object($rtval)) {
                $mod_forums_array = $rc->forum_id;
                $PHORUM["admin_user"]["forums"][] = $rc->forum_id;
            }
            putenv("admin_user_id=$admin_user_id");
            putenv("mod_forums_array=$mod_forums_array");
        }
    }
    return $uid;
  }
}

class DS_Default_Auth extends DS_Auth {
  var $classname = "DS_Default_Auth";

  var $nobody    = true;
}

class DS_Perm extends Perm {
  var $classname = "DS_Perm";

  var $permissions = array(
                            "user"       => 1,
                            "author"     => 2,
                            "editor"     => 4,
                            "supervisor" => 8,
                            "admin"      => 16
                          );

  function perm_invalid($does_have, $must_have) {
    global $perm, $auth, $sess;
    global $_PHPLIB;

    include($_PHPLIB["libdir"] . "perminvalid.ihtml");
  }
}
?>
