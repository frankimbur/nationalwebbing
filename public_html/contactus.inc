<?
# Certain column names are always excluded from end user alteration in any form:
$excludeSQL= '
    "uid",
    "username",
    "password",
    "perms",
    "ipaddress",
    "receiveupdates",
    "instantedit",
    "active",
    "timesloggedon",
    "lastlogon"';

$excludeArray=array(
    "uid",
    "username",
    "password",
    "perms",
    "ipaddress",
    "receiveupdates",
    "instantedit",
    "active",
    "timesloggedon",
    "lastlogon");

#########################
Function CheckContactFormTable()
#########################
{
    global $excludeArray;
    ## make sure contactform helper table has rows...
    $result=DoQuery("DESCRIBE mailinglist");
    while ($CurrentRecord=mysql_fetch_array($result)) {
        $colname=$CurrentRecord[0];
        if (In_Array($colname,$excludeArray))
        if (! OneSQLVALUE("SELECT contactform_no FROM contactform WHERE columnname='$colname'") ) {
            $Label = ucfirst($colname);
            $Label = ereg_replace("_"," ",$Label);
            DoQuery("INSERT INTO contactform SET columnname='$colname',display_text='$Label'");
        }
    }
}


?>
