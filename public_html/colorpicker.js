function FCKShowDialog(pagePath, args, width, height)
{
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;help:no;scroll:no;status:no");
}

function foreColor( field )
{
        url = "fckeditor/dialog/fck_selcolor.php?initcolor=" + field.value.substring(1);

	var color = FCKShowDialog(url, "", 370, 240);
        if( color.length > 0 ) {
          field.value = color;
        }
}