<?php
require( "prepend.php3" );

$post_fieldnames = array();
$post_values = array();

$builtin=
   '"uid",
    "username",
    "password",
    "perms",
    "ipaddress",
    "receive_email",
    "receiveupdates",
    "instantedit",
    "active",
    "lastlogon",
    "timesloggedon"';

$result = mysql_query( "select * from contactform where columnname not in ($builtin)" );

while( $row = mysql_fetch_assoc( $result ) )
{
  // avoid invalid column names:  
  $row["columnname"] = stripslashes(strtolower($row["columnname"]));
  $row["columnname"] = str_replace("&","",$row["columnname"]);
  $row["columnname"] = str_replace("\"","",$row["columnname"]);
  $row["columnname"] = str_replace("'","",$row["columnname"]);
  $row["columnname"] = str_replace("<","",$row["columnname"]);
  $row["columnname"] = str_replace(">","",$row["columnname"]);
  $row["columnname"] = str_replace("-","_",$row["columnname"]);
  $row["columnname"] = str_replace(" ","_",$row["columnname"]);
  $colname=$row["columnname"];
  
  array_push( $post_fieldnames, strtolower($row["columnname"]) );
}

array_push( $post_fieldnames, "field_username" );
array_push( $post_fieldnames, "field_receiveupdates" );
array_push( $post_fieldnames, "suggestions" );

foreach( $post_fieldnames as $fieldname )
{
  if( $fieldname != "field_username" && $fieldname != "field_receiveupdates" && $fieldname != "suggestions" )
  {
    $postfield = "field_$fieldname";
  }
  else
  {
    $postfield = $fieldname;
  }
  
  $fieldtype = OneSQLValue( "select field_type from contactform where columnname = '$fieldname'" );
  
  if( $fieldtype == 'D' && !empty( $HTTP_POST_VARS[$fieldname."month"] ) )
  {
    $addtoarray = $HTTP_POST_VARS[$fieldname."year"]."-".$HTTP_POST_VARS[$fieldname."month"]."-".$HTTP_POST_VARS[$fieldname."day"];
  }
  elseif( $fieldtype == 'T' && !empty( $HTTP_POST_VARS[$fieldname."hour"] ) )
  {
    if( $HTTP_POST_VARS[$fieldname."ampm"] == "PM" && $HTTP_POST_VARS[$fieldname."hour"] < 12 )
    {
      $HTTP_POST_VARS[$fieldname."hour"] += 12;
    }
    elseif( $HTTP_POST_VARS[$fieldname."ampm"] == "AM" && $HTTP_POST_VARS[$fieldname."hour"] == 12 )
    {
      $HTTP_POST_VARS[$fieldname."hour"] -= 12;
    }
    
    $addtoarray = $HTTP_POST_VARS[$fieldname."hour"].":".$HTTP_POST_VARS[$fieldname."minute"].":00";
  }
  elseif( $fieldtype == 'M' )
  {
    if( count( $HTTP_POST_VARS[$postfield] ) )
    {
      $addtoarray = implode( "\n", $HTTP_POST_VARS[$postfield] );
    }
    else
    {
      $addtoarray = "";
    }
  }
  else
  {
    $addtoarray = $HTTP_POST_VARS[$postfield];
  }
  
  array_push( $post_values, $addtoarray );
}

for( $i=0; $i<count($post_values); $i++ )
{
  if( $post_fieldnames[$i] != "suggestions" && $post_fieldnames[$i] != "submit" )
  {
    $qs_values .= "'$post_values[$i]', ";
  
    if( stristr( $post_fieldnames[$i], "field_" ) )
    {
      $qs_fieldnames .= substr( $post_fieldnames[$i], 6 ) . ", ";
    }
    else
    {
      $qs_fieldnames .= "$post_fieldnames[$i], ";
    }
  }
}

$qs_fieldnames = substr( $qs_fieldnames, 0, strlen($qs_fieldnames)-2 );
$qs_values = substr( $qs_values, 0, strlen($qs_values)-2 );

$query = "insert into mailinglist ( " . $qs_fieldnames . " ) values ( " . $qs_values . " )";
$result = mysql_query( $query );

$send_to_email = OneSQLValue( "select sales_emailid from sitesetup where site_no = 1" );

for( $i=0; $i<count($post_fieldnames); $i++ )
{
  $message .= <<<END
$post_fieldnames[$i]:
$post_values[$i]
\n
END;
}

mail( $send_to_email, "Contact Form Submission", $message );

$template_no = OneSQLValue( "select module_template_no from contactusoption where contactusoption_no = 1" );

DSBeginPage("Data Received.",0,1,$template_no);
echo <<<END
<font color="#888888">Thanks for your feedback! Your data was sent successfully.</font>
END;
DSEndPage(0,0,$template_no);
?>