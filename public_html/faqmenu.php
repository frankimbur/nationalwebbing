<?php
########################################################################
# Copyright 1999 Desktop Solutions Software, Inc.
# 516-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
require ("prepend.php3");
page_open(
array("sess" => "DS_Session",
"auth" => "DS_Auth",
"perm" => "DS_Perm"));
if (!HasPermissions("admin")) {
    return;
}
#$DSDEBUG=1;
$module_template_no = 999;  // use admin template
$systemname = OneSQLValue( "select systemname from faqoption where faqoption_no = 1" );
DSBeginPage("$systemname Menu",0,1,$module_template_no);
StartDCMenu("$systemname Menu");
## Display products
DrawDCMenuLine("$systemname Actions");
## DrawDCMenuLine("$heading Menu");
DrawDCMenuLine("Maintain Items","faq_browse.dtop");
DrawDCMenuLine("Program Options","faqoptions.dtop");
DrawDCMenuLine("View $systemname page","faq.html");
DrawDCMenuLine("Maintain Categories","faq_category_browse.dtop");
DrawDCMenuLine("Maintain Feedback Submissions","faq_userposts_browse.dtop");
DrawDCMenuLine("$systemname Teaser Content","faqteaseroptions.dtop");
DrawDCMenuLine("Other Options");
DrawDCMenuLine("View $systemname Documentation","http://www.desktopmodules.com/docs/qanda.pdf");
DrawDCMenuLine("Return to main menu",'admin.html');
EndDCMenu();

DSEndPage(1,0,$module_template_no); ##CET removed skip extra and added template
?>
