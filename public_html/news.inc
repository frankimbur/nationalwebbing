<?php
#################
function MostRecentWriting()
#################
{
    global $DSTABLEWIDTH,$DSUPLOADURL,$DSWINDOWBG,$DSTABLESELLSPACING,$DSTABLECELLPADDING,
           $DSHEADINGBG,$DSHEADINGFG,$DSALTCOLDATABG,$DSCOLDATABG;
    $title=OneSQLValue("SELECT title FROM news ORDER BY dte_news DESC LIMIT 1");
    $key=OneSQLValue("SELECT news_no FROM news ORDER BY dte_news DESC LIMIT 1");
        echo "
        <TABLE WIDTH=150 BGCOLOR=$DSWINDOWBG CELLSPACING=$DSTABLECELLSPACING CELLPADDING=$DSTABLECELLPADDING>
        <TR><TD BGCOLOR=$DSHEADINGBG ALIGN=CENTER><span class=sidebar>Writings</span></TD></TR>
        <TR><TD BGCOLOR=$DSCOLDATABG><span class=sidebar><A HREF=newsdetail.html?key=$key>$title</a><br></span</TD></TR>
        <TR><TD BGCOLOR=$DSHEADINGBG ALIGN=CENTER><span class=sidebar><A HREF=writings.html>More writings...</a></span></TD></TR>
        </TABLE>";

}
#################
function DrawNews($width=0,$worktype,$workcat,$searchfor)
#################
#MODIFIED FOR UU
{
    global $DSTABLEWIDTH,$DSUPLOADURL,$DSWINDOWBG,$DSTABLESELLSPACING,$DSTABLECELLPADDING,
           $DSHEADINGBG,$DSHEADINGFG,$DSALTCOLDATABG,$DSCOLDATABG;
    if (!$width) {
        $width=$DSTABLEWIDTH;
    }
    $heading = NewsField("news_heading");
    if (strlen($worktype)) {
        $where=" AND worktype='$worktype'";
        $heading = OneSQLValue("SELECT text FROM code WHERE code='$worktype' AND category='worktype'");
        $back=1;
    }
    if (strlen($workcat)) {
        $where.=" AND workcategory='$workcat'";
        $heading .= "<BR>".OneSQLValue("SELECT text FROM code WHERE code='$workcat' AND category='workcat'");
        $back=1;
    }
    if (strlen($searchfor)) {
        $where="AND (text like '%$searchfor%' OR title like '%$searchfor%')";
        $heading = "Works containing <b><i>$searchfor</i></b>";
        $back=1;
    }
    global $DSDEBUG;
    #$DSDEBUG=1;
    $result=DoQuery("SELECT DATE_FORMAT(dte_news,'%b %d, %Y') as cdate,news_no,dte_news,title,news_filename,text from news WHERE active='Y' $where order by dte_news DESC");
    echo "
    <TABLE WIDTH=$width BGCOLOR=$DSWINDOWBG CELLSPACING=$DSTABLECELLSPACING CELLPADDING=$DSTABLECELLPADDING>
    <TR><TD COLSPAN=2 BGCOLOR=$DSHEADINGBG ALIGN=CENTER><FONT COLOR=$DSHEADINGFG>".
    $heading."</TD></TR>";
    while ($CurrentRecord=mysql_fetch_object($result)) {
        if (strlen($CurrentRecord->text)) {
            $url="<a href=newsdetail.html?key=$CurrentRecord->news_no&back=$back>$CurrentRecord->title</a>";
#           "<FONT SIZE=-2>".substr(trim(strip_tags($CurrentRecord->text)),0,80)." <a href=newsdetail.html?key=$CurrentRecord->news_no>more...</a></FONT>";
        }
        else {
              $url="<a href=\"$DSUPLOADURL$CurrentRecord->news_filename\">$CurrentRecord->title</a><BR>";
#             "<FONT SIZE=-2>".$CurrentRecord->news_filename." <a href=\"$DSUPLOADURL$CurrentRecord->news_filename\">more...</a></FONT>";
        }
        if ($color==$DSCOLDATABG)
            $color=$DSALTCOLDATABG;
        else
            $color=$DSCOLDATABG;
        echo "
        <TR>
        <TD BGCOLOR=$color>$CurrentRecord->cdate</TD>
        <TD BGCOLOR=$color>$url</TD>
        </TR>
        ";
    }
    echo "
    <TR><TD COLSPAN=2 ALIGn=CENTER BGCOLOR=$DSHEADINGBG>&nbsp;<FONT COLOR=$DSHEADINGFG></FONT></TD></TR>
    </TABLE>";
}
#######################
function DrawWritings()
#######################
# JUST FOR UU
{
    echo "
    <H1>".NewsField("news_heading")."</H1>
    <form action=news.html>
    <table border=0 width=100%>
    <tr><td>Search ".NewsField("news_heading").":</TD></tr>
    <TR><td><input type=text width=20 name=searchfor> <input type=submit value=' Go '></td></tr>
    </table></form>
    Or select from one of the options below:<BR>
    <UL>
    ";
    $handle=DoQuery("SELECT * from code WHERE category='worktype' ORDER BY cardinal");
        while ($CurrentRecord=mysql_fetch_object($handle)) {
            echo "<LI><a href=news.html?worktype=$CurrentRecord->code>$CurrentRecord->text</A><BR>";
            $h2=DoQuery("
                SELECT DISTINCT workcategory,code.text as text
                FROM news,code
                WHERE worktype='$CurrentRecord->code'
                AND news.workcategory=code.code
                AND code.category='workcat' ORDER BY text");
                while ($CurrentRecord2=mysql_fetch_object($h2)) {
                    echo "<UL><LI><A href=news.html?workcat=$CurrentRecord2->workcategory&worktype=$CurrentRecord->code>$CurrentRecord2->text</A></LI></UL>";
                }
        }
    echo "</UL>";
}

?>
