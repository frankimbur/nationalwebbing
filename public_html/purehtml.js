function FCKShowDialog(pagePath, args, width, height)
{
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;help:no;scroll:no;status:no");
}

function PureHTML( field )
{
        url = "purehtml.html";

	var content = FCKShowDialog(url, "", 520, 260);
        if( content.length > 0 ) {
          field.value = content;
        }
}