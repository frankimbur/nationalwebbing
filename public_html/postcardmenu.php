<?
########################################################################
# Copyright 2001 Desktop Solutions Software, Inc.
# 631-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
# postcardmenu.php - program file for the postcard software
require ("prepend.php3");
page_open(array("sess" => "DS_Session",
				"auth" => "DS_Auth",
				"perm" => "DS_Perm"));
if (!HasPermissions("admin")) {
    return;
}


$module_template_no=OneSQLValue("SELECT module_template_no FROM postcardoption WHERE postcardoption_no=1");

$DSPUREHTML=1;

require "postcard.inc";
require "filter.inc";



$HOW_TO_SEND_MAIL    = "MAIL";     // MAIL FOR MAIL FUNC, {SOCKET FOR SOCKET CONNECTION}, ATTACHMENTS USE MAIL ONLY
$MAIL_PORT           = 25;         // smtp mail port
$SLEEP_AFTER_MAILS   = 20;         // number of mails to go by before sleep, if 0, no sleep
$TIME_TO_SLEEP_FOR   = 2;          // seconds to sleep for
$MAX_ATTACHMENT_SIZE = 100000;      // 100K The maximum size attachment allowed
$SCRIPT_TIMEOUT      = 21600;      // seconds before timeout  (6 hours)
                                   // TM: I can send 1500 emails easily under norual timeout
                                   // with a lan-attached smtp server
                                   // TM: attachments are much slower, without timeout set at 2000
                                   // and 100K attachment, I only received 168 emails, should have
                                   // received 1500
                                   // also note that the tmp directory of the smtp server
                                   // must be able to accomodate all of the attachments
set_time_limit($SCRIPT_TIMEOUT);
$systemname = OneSQLValue( "select systemname from postcardoption where postcardoption_no = 1" );
$module_template_no = 999;  // use admin template
if ($Submit=='Preview') {
    PreviewEmail();
}
else {
    switch($action) {
        case "compose":
            DSBeginPage("$systemname: Compose",0,1,$module_template_no);
            ComposeEmail();
            DSEndPage(1,0,$module_template_no);
            break;
        case "sendemail":
            SendNewsLetter();
            break;
        default:
            $systemname = OneSQLValue( "select systemname from postcardoption where postcardoption_no = 1" );
            DSBeginPage("$systemname Menu",0,1,$module_template_no);
            StartDCMenu("$systemname Menu");
            DrawDCMenuLine("$systemname Actions");
            DrawDCMenuLine("Compose",'postcardmenu.php?action=compose');
            DrawDCMenuLine("View Archives, re-send items",'postcardbrowse.php');
            DrawDCMenuLine("Browse Subscriber List","mailinglist_browse.dtop");
            DrawDCMenuLine("Download Subscriber List",'download.dtop?table=mailinglist');
            DrawDCMenuLine("Upload Subscriber List",'importcsv.dtop?tablename=mailinglist');
            DrawDCMenuLine("Paste Emails",'pastemails.dtop');
            DrawDCMenuLine("ZAP Mailing List",'zapemails.dtop');
            DrawDCMenuLine("Filters","filter_browse.dtop?filter_type=P");
            DrawDCMenuLine("Program Options",'postcardoption_edit.php');
            DrawDCMenuLine("View Subscription Page",'postcardsubscription.php');
            DrawDCMenuLine("Other Options");
            DrawDCMenuLine("View $systemname Documentation",'http://www.desktopmodules.com/docs/postcardexpress.pdf');
            DrawDCMenuLine("Return to main menu",'admin.html');
            EndDCMenu();
            DSEndPage(1,0,$module_template_no);
    }
}

?>
