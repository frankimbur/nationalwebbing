<?php
########################################################################
# Copyright 2001 Desktop Solutions Software, Inc.
# 631-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
# $Id: move_files.inc,v 1.1.1.1 2001/11/28 20:11:54 tom Exp $
#require ("prepend.php3");
#page_open(array("sess" => "DS_Session"
#				,"auth" => "DS_Auth",
#				"perm" => "DS_Perm"
#				));
#########################################################################
#########################################################################
# move_files.inc- include file for file move
##############
# NOTE: You CAN NOT use DoQuery in this file, because we are dealing with
# two databases.  Use the generic mysql functions.



########
# Function DropTables($database,$noexclude=0)
# drop all tables except those defined in $exclude_tables from $database
########
Function DropTables($database,$noexclude=0)
{
    global $exclude_tables,$DSHOST,$DSUSER,$DSPASSWORD;
    $table_to_be_dropped = "";
    $counter=0;

    //connect
    $link = mysql_connect($DSHOST,$DSUSER,$DSPASSWORD) or die ("Could not connect: DropTables");

    //select correct db
    $selectdb = mysql_select_db($database,$link);

    //get tables in database
    $tables=mysql_list_tables($database);

    //loop through, drop tables not in array
    while ($tbl=mysql_fetch_array($tables)) {
        $table_to_be_dropped = $tbl[0];
        if (!$noexclude) {
            if (!in_array($table_to_be_dropped,$exclude_tables)) {
                $q = "DROP TABLE $table_to_be_dropped";
                if ($DSDEBUG) {
                    echo $q . "<br>";
                }
                $counter++;
                $r = mysql_query($q);
#                DoQuery($q);
            }
        }
        else {
                $q = "DROP TABLE $table_to_be_dropped";
                if ($DSDEBUG) {
                    echo $q . "<br>";
                }
                $counter++;
                $r = mysql_query($q);
#                DoQuery($q);
        }
    }

    if ($DSDEBUG) {
        echo "<br>$counter tables dropped";
    }



}

########
# Function MoveTables($mode)
# move table from one database to another
########
Function MoveTables($mode)
{

    global $db, $stagingdb;

    switch(strtoupper($mode)) {
        case "PUBLISH":
                //move tables from staging to production
                flush();
                DropTables($db);
                CreateTables("PUBLISH");

                break;
        case "REVERT":
                //move tables from production to staging
                flush();
                DropTables($stagingdb);
                CreateTables("REVERT");

                break;
        default:
            echo "Error" . $mode;

    }


}

###########
# Function GetTableSchemas($database,$link_id,$noexclude=0)
# get schemas of 1st database
###########
Function GetTableSchemas($database,$link_id,$noexclude=0)
{
    global $exclude_tables,$DSDEBUG;

    mysql_select_db($database,$link_id);

    //get tables in database
    $tables=mysql_list_tables($database);
    $schema = array();
    $counter=0;

    //loop through, get schemas for tables not in array
    while ($tbl=mysql_fetch_array($tables)) {
        if (!$noexclude) {
            if (!in_array($tbl[0],$exclude_tables)) {
                $schema[$counter] = get_table_def($tbl[0] . "\n",$database);
                if ($DSDEBUG) {
                    echo $tbl[0] ."<br>".$schema[$counter] . "<br>";
                    print_r($tbl);
                    echo "foobar";
                }
                $counter++;
#                DoQuery($schema);
            }
        }
        else {

                $schema[$counter] = get_table_def($tbl[0] . "\n",$database);
                if ($DSDEBUG) {
                    echo $tbl[0] ."<br>".$schema[$counter] . "<br>";
                    print_r($tbl);
                    echo "foobar";
                }
                $counter++;
#                DoQuery($schema);
       }

    }


    if ($DSDEBUG) {
        echo "<br>$counter schemas created";
    }

    return $schema;

}


########
# Function CreateTables($mode)
# creates tables in database based mode, moves data
# from first to second database
########
Function CreateTables($mode)
{
    global $stagingdb,$db,$DSHOST,$DSUSER,$DSPASSWORD,$DSDEBUG;

    $link = mysql_connect($DSHOST,$DSUSER,$DSPASSWORD) or die ("Could not connect");

    switch(strtoupper($mode)) {
        case "PUBLISH"://move tables from staging to production

                //get schemas from staging db
                $schemas = GetTableSchemas($stagingdb,$link);

                //select production db
                $selecteddb = mysql_select_db($db);

                //create tables on production server
                for ($i=0;$i<count($schemas);$i++) {
                    if ($DSDEBUG) {
                        echo "CreateTables:<br>";
                        echo $schemas[$i] . "<br>";
                    }
                    $result = mysql_query($schemas[$i],$link);
                }

                //move data
                $tables = GetTables($stagingdb);
                for ($i=0;$i<count($schemas);$i++) {
                    $q = "INSERT INTO ".$db.".".$tables[$i]." SELECT * FROM ".$stagingdb.".".$tables[$i];
                    if ($DSDEBUG) {
                        echo $q . "<br>";
                    }
                    $result = mysql_query($q,$link);
                }

                break;
        case "REVERT": //move tables from production to staging

                //get schemas for production db
                $schemas = GetTableSchemas($db,$link);

                //select staging db
                $selecteddb = mysql_select_db($stagingdb);

                //create tables on staging server
                for ($i=0;$i<count($schemas);$i++) {
                    if ($DSDEBUG) {
                        echo "CreateTables:<br>";
                        echo $schemas[$i] . "<br>";
                    }
                    $result = mysql_query($schemas[$i],$link);
                }

                //move data
                $tables = GetTables($db);
                for ($i=0;$i<count($schemas);$i++) {
                    $q = "INSERT INTO ".$stagingdb.".".$tables[$i]." SELECT * FROM ".$db.".".$tables[$i];
                    if ($DSDEBUG) {
                        echo $q . "<br>";
                    }
                    $result = mysql_query($q,$link);
                }

                break;
        default:
            echo "Error" . $mode;

    }

}


########
# Function GetTables($db,$noexclude=false)
# returns an array with the table names, minus
# if noexclude is false, the table names in
# $exclude_tables are omitted
########
Function GetTables($database,$noexclude=false)
{

    global $stagingdb,$db,$DSHOST,$DSUSER,$DSPASSWORD,$DSDEBUG,$exclude_tables;

    $tables = array();
    $counter=0;
    $link = mysql_connect($DSHOST,$DSUSER,$DSPASSWORD) or die ("Could not connect");
    mysql_select_db($database,$link);

    //get tables in database
    $r=mysql_list_tables($database);
    while ($tbl = mysql_fetch_array($r)) {
        if (!$noexclude) {
            if (!in_array($tbl[0],$exclude_tables)) {
                $tables[$counter] = $tbl[0];
                $counter++;
            }

        }
        else {
            $tables[$counter] = $tbl[0];
            $counter++;
        }
    }

    return $tables;
}




function get_table_def($table, $db,$crlf="\n")
{
#    global $drop;
#
#    $schema_create = "";
#    if(!empty($drop))
#        $schema_create .= "DROP TABLE IF EXISTS $table;$crlf";

    $schema_create .= "CREATE TABLE $table ($crlf";

    $result = mysql_db_query($db, "SHOW FIELDS FROM $table") or mysql_die();
#     $result = DoQuery("SHOW FIELDS FROM $table");
    while($row = mysql_fetch_array($result))
    {
        $schema_create .= "   $row[Field] $row[Type]";

        if(isset($row["Default"]) && (!empty($row["Default"]) || $row["Default"] == "0"))
            $schema_create .= " DEFAULT '$row[Default]'";
        if($row["Null"] != "YES")
            $schema_create .= " NOT NULL";
        if($row["Extra"] != "")
            $schema_create .= " $row[Extra]";
        $schema_create .= ",$crlf";
    }
    $schema_create = ereg_replace(",".$crlf."$", "", $schema_create);
    $result = mysql_db_query($db, "SHOW KEYS FROM $table") or mysql_die();
#    $result = DoQuery("SHOW KEYS FROM $table");
    while($row = mysql_fetch_array($result))
    {
        $kname=$row['Key_name'];
        if(($kname != "PRIMARY") && ($row['Non_unique'] == 0))
            $kname="UNIQUE|$kname";
         if(!isset($index[$kname]))
             $index[$kname] = array();
         $index[$kname][] = $row['Column_name'];
    }

    while(list($x, $columns) = @each($index))
    {
         $schema_create .= ",$crlf";
         if($x == "PRIMARY")
             $schema_create .= "   PRIMARY KEY (" . implode($columns, ", ") . ")";
         elseif (substr($x,0,6) == "UNIQUE")
            $schema_create .= "   UNIQUE ".substr($x,7)." (" . implode($columns, ", ") . ")";
         else
            $schema_create .= "   KEY $x (" . implode($columns, ", ") . ")";
    }

    $schema_create .= "$crlf)";
    return (stripslashes($schema_create));
}






########
# Function FloodBuffer()
# send data to the browser to create a new
# page during the file copy
########
Function FloodBuffer()
{
        global $DSDEBUG;
        echo "<html>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            ";
        echo "<pre>";
            if ($DSDEBUG==1) {
                echo "<tr><td>";
                echo $action;
                echo "</td></tr>";
            }


}

?>
