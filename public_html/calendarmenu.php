<?php
########################################################################
# Copyright 1999 Desktop Solutions Software, Inc.
# 516-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
require ("prepend.php3");
page_open(
array("sess" => "DS_Session",
"auth" => "DS_Auth",
"perm" => "DS_Perm"));
if (!HasPermissions("admin")) {
    return;
}
$module_template_no = 999;  // use admin template
$systemname = OneSQLValue( "select systemname from calendaroption where calendaroption_no = 1" );
DSBeginPage("$systemname Menu",0,1,$module_template_no);
StartDCMenu("$systemname Menu");
DrawDCMenuLine("$systemname Actions");
DrawDCMenuLine("Maintain Items","calendar_messages_browse.dtop");
DrawDCMenuLine("Delete old Items","calendar_delete.dtop");
DrawDCMenuLine("Program Options","calendaroptions.dtop");
DrawDCMenuLine("View $systemname","calendar.html");
DrawDCMenuLine("$systemname Teaser Content","calendarteaseroptions.dtop");
DrawDCMenuLine("Other Options");
DrawDCMenuLine("View $systemname documentation",'http://www.desktopmodules.com/docs/ecalendar.pdf');
DrawDCMenuLine("Return to main menu",'admin.html');
EndDCMenu();

DSEndPage(1,0,$module_template_no); ##CET removed skip extra and added template
?>
