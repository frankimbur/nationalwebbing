<?php

################################################################
## VALIDATE_SEARCH Function
################################################################
function validate_search( $search_urls )
{
  $search_urls = explode( "\n", $search_urls );

  $emptyspace = str_repeat( " ", 2048 );
  echo $emptyspace;
  
  echo <<<END
<HTML>
<HEAD>
  <TITLE>Validating...</TITLE>
</HEAD>
<BODY style="font-family:arial;font-size:11px;">
<a style="font-size:14pt;"><b>WARNING: DO NOT CLOSE THIS WINDOW OR LEAVE THIS PAGE</b></a><br>
Doing so will result in an incomplete set of cache files, which can cause serious problems when performing searches.<p>
END;

  $url_error = false;

  $d = dir( "searchcache" );

  while (false !== ($entry = $d->read()))
  {
    if( $entry != "." && $entry != ".." )
    {
      unlink( "searchcache/$entry" );
    }
  }

  $d = dir( "searchurls" );

  while (false !== ($entry = $d->read()))
  {
    if( $entry != "." && $entry != ".." )
    {
      unlink( "searchurls/$entry" );
    }
  }

  foreach( $search_urls as $url )
  {
    echo "<b>$url</b><br>";
    echo "Validating............ ";
    
    if( @fopen( $url, 'r' ) == false )
    {
      $url_error = true;
      echo "<font color=\"#FF0000\"><b>Failed!</b><br>\n";
      echo "<i>See bottom of page for additional information.</i></font><br><br>\n";
    }
    else
    {
      echo "<font color=\"#006600\"><b>OK</b></font> &nbsp; &nbsp; \n";
      echo "Creating new cache files............ ";
      
      $contents = implode( "", file($url) );
      $tempname = tempnam( "searchcache", "w" );
      $handle = fopen( $tempname, "w" );
      fwrite( $handle, $contents );
      fclose( $handle );

      $tempname = basename( $tempname );
      
      $handle = fopen( "searchurls/$tempname", "w" );
      fwrite( $handle, $url );
      fclose( $handle );
      
      echo "<b>Done</b><br>\n";
      
      echo <<<END
</BODY>
</HTML>
END;

    }
  }

  if( $url_error )
  {
    echo "<font color=\"#FF0000\">&nbsp;<br><b>One or more URLs were Invalid</b><br>Make sure that there is only one per line, that each is prepended with 'http://', and that each points to an existing page.</font><p><a href=\"javascript:history.go(-1)\">Back</a>";
    exit();
  }
}



################################################################
## SEARCH_URL Function
################################################################
function search_url( $searchstring )
{
  if( strlen( $searchstring ) < 3 )
  {
    return "Your search string must be at least three characters long.";
  }

  $original_searchstring = $searchstring;

  $foundanything = false;
  $counter = 0;
  
  $d = dir( "searchcache" );
  
  while (false !== ($filename = $d->read()))
  {
    if( $entry != "." && $entry != ".." )
    {
      $contents = implode( "", file("searchcache/$filename") );
  
      $pos1 = strpos( $contents, "<TITLE>" );
    
      if( $pos1 === false  ) {
        $pos1 = strpos( $contents, "<title>" );
      }
    
      $pos1 += 7;
    
      $title = substr( $contents, $pos1 );
    
      $pos2 = strpos( $title, "</TITLE>" );
    
      if( $pos2 === false ) {
        $pos2 = strpos( $title, "</title>" );
      }
    
      $title = substr( $title, 0, $pos2 );
    
      while( strpos( $contents, "<style" ) || strpos( $contents, "<STYLE" ) )
      {
        $stylepos1 = strpos( $contents, "<style" );
    
        if( $stylepos1 === false )
        {
          $stylepos1 = strpos( $contents, "<STYLE" );
        }
    
        $stylepos2 = strpos( $contents, "</style>" );
    
        if( $stylepos2 === false )
        {
          $stylepos2 = strpos( $contents, "</STYLE>" );
        }
    
        if( $stylepos1 !== false && $stylepos2 !== false )
        {
          $contents = substr( $contents, 0, $stylepos1 ) . substr( $contents, $stylepos2 + 8 );
        }
      }
    
      while( strpos( $contents, "<script" ) || strpos( $contents, "<SCRIPT" ) )
      {
        $scriptpos1 = strpos( $contents, "<script" );
    
        if( $scriptpos1 === false )
        {
          $scriptpos1 = strpos( $contents, "<SCRIPT" );
        }
    
        $scriptpos2 = strpos( $contents, "</script>" );
    
        if( $scriptpos2 === false )
        {
          $scriptpos2 = strpos( $contents, "</SCRIPT>" );
        }
    
        if( $scriptpos1 !== false && $scriptpos2 !== false )
        {
          $contents = substr( $contents, 0, $scriptpos1 ) . substr( $contents, $scriptpos2 + 9 ); 
        }
        else
        {
          break;
        }
      }
    
      #$outcontent = htmlspecialchars( $contents );
      #echo "<pre>$outcontent</pre><p>";
    
      $contents = str_replace( "<br>", " ", $contents );
      $contents = str_replace( "<BR>", " ", $contents );
      $contents = str_replace( "<p>", " ", $contents );
      $contents = str_replace( "<P>", " ", $contents );
      $contents = str_replace( "<td>", " ", $contents );
      $contents = str_replace( "<TD>", " ", $contents );
      $contents = str_replace( "</td>", " ", $contents );
      $contents = str_replace( "</TD>", " ", $contents );
      $contents = str_replace( "<H1>", " ", $contents );
      $contents = str_replace( "<H2>", " ", $contents );
      $contents = str_replace( "<H3>", " ", $contents );
      $contents = str_replace( "<H4>", " ", $contents );
      $contents = str_replace( "<H5>", " ", $contents );
      $contents = str_replace( "<H6>", " ", $contents );
      $contents = str_replace( "<H7>", " ", $contents );
      $contents = str_replace( "</H1>", " ", $contents );
      $contents = str_replace( "</H2>", " ", $contents );
      $contents = str_replace( "</H3>", " ", $contents );
      $contents = str_replace( "</H4>", " ", $contents );
      $contents = str_replace( "</H5>", " ", $contents );
      $contents = str_replace( "</H6>", " ", $contents );
      $contents = str_replace( "</H7>", " ", $contents );
      $contents = str_replace( "<LI>", " ", $contents );
    
      $contents = strip_tags( $contents );
      $contents = trim( $contents );
    
      $contents = str_replace( "\n", "", $contents );
      $contents = str_replace( "\r", "", $contents );
      $contents = str_replace( "  ", " ", $contents );
      $contents = str_replace( "&nbsp;", " ", $contents );
    
      $terms .= $searchstring;
    
      $termslower = strtolower( $searchstring );
      $terms .= " $termslower";
    
      $termsupper = strtoupper( $searchstring );
      $terms .= " $termsupper";
    
      $searchstring = strtolower( $searchstring );
      $termswords = ucwords( $searchstring );
      $terms .= " $termswords";
    
      $terms = explode( " ", $terms );
      
      $terms = array_filter( $terms, "common_words" );
      
      /*
      echo "<pre>";
      print_r( $terms );
      echo "</pre><p>";
      */
    
      $foundtitle = false;
      $foundcontent = false;
    
      foreach( $terms as $term )
      {
        if( strlen( $term ) >= 2 )
        {
          $term = " $term";
          
          $title = " $title ";
    
          if( stristr( $title, $term ) )
          {
            $term = trim($term);
            $title = trim( str_replace( $term, "<b>$term</b>", $title ) );
            $foundtitle = true;
            $foundanything = true;
          }
        }
      }
    
      foreach( $terms as $term )
      {
        if( strlen( $term ) >= 2 )
        {
          $term = " $term";
          
          $contents = " $contents ";
    
          if( stristr( $contents, $term ) )
          {
            $term = trim($term);
            $contents = trim( str_replace( $term, "<b>$term</b>", $contents ) );
            $foundcontent = true;
            $foundanything = true;
          }
        }
      }
    
      if( $foundcontent == true )
      {
        $pos1 = strpos( $contents, "<b>" );
    
        if( $pos1 < 250 ) {
          $pos1 = 0;
        } else {
          $pos1 -= 250;
          $pos1 = strpos( $contents, " ", $pos1 );
        }
    
        if( $pos1+500 <= strlen( $contents ) )
        { 
          $pos2 = strpos( $contents, " ", $pos1+500 );
        }
        else
        {
          $pos2 = strlen( $contents );
        }
    
        $contentlength = strlen( $contents );
        $contents = substr( $contents, $pos1, $pos2-$pos1 );
    
        if( ( $contentlength - $pos1 ) > strlen( $contents ) )
        {
          $contents .= "...";
        }
      }
      elseif( $foundcontent == false && $foundtitle == true )
      {
        $contents = substr( $contents, 0, 500 );
      }
    
      if( $foundcontent == true || $foundtitle == true )
      {
        $url = implode( "", file("searchurls/$filename") );
        
        $counter++;
        
        $returnval .= <<<END
    <b>$counter.</b> <a href="$url">$title</a><br>
    $contents<P>
END;
      }
    }
  }

  if( $foundanything == false )
  {
    $err = "<b>No results matched your query.</b>";
    return $err;
  }
  else
  {
    $returnval .= <<<END
<div align="right"><a href="sitesearch.html">Start a new search</a><p></div>
END;

    if( $counter > 1 ) {
      $s = "s";
    }

    $returnval = "Found <b>$counter</b> Result$s for <i>$original_searchstring</i><p>\n" . $returnval;

    return $returnval;
  }
}



function common_words( $word )
{
  if( strlen($word) < 3 )
  {
    return false;
  }

  if( !strcmp( strtolower($word), "for" ) )
  {
    return false;
  }
  
  if( !strcmp( strtolower($word), "from" ) )
  {
    return false;
  }
  
  if( !strcmp( strtolower($word), "into" ) )
  {
    return false;
  }
  
  if( !strcmp( strtolower($word), "the" ) )
  {
    return false;
  }
  
  return true;
}

?>