<?php
########################################################################
# Copyright 2001 Desktop Solutions Software, Inc.
# 631-493-3422
# info@desktopsolutions.com
# www.desktopsolutions.com
# usage rights granted for use on a per-project-basis
#########################################################################
require ("prepend.php3");
page_open(array("sess" => "DS_Session",
				"auth" => "DS_Auth",
				"perm" => "DS_Perm"));
if (!HasPermissions("admin")) {
    return;
}
require "newsletter.inc";
#########################################################################
$form = new DSForm;

$key=1; // hard-coded to always edit a single record..

$CurrentRecord = ReadCurrentRecord('newsletteroption','newsletteroption_no',$key);

if (isset($key)) {
    $form->add_element(array(
        "type"=>"hidden",
        "name"=>"key",
        "value"=>$key
        ));
}

$form->add_element(array(
"type"=>"text",
"name"=>"field_systemname",
"value"=>$CurrentRecord->systemname,
"size"=>36,
"maxlength"=>40,
"minlength"=>1,
"length_e"=>"Please enter the system name field."
));

$form->add_element(array(
"type"=>"textarea",
"name"=>"field_html_signature",
"value"=>$CurrentRecord->html_signature,
"size"=>0,
"rows"=>6,
"cols"=>60
));

$form->add_element(array(
"type"=>"textarea",
"name"=>"field_html_subscribe",
"value"=>$CurrentRecord->html_subscribe,
"size"=>0,
"rows"=>6,
"cols"=>60
));

$form->add_element(array(
"type"=>"text",
"name"=>"field_from_address",
"value"=>$CurrentRecord->from_address,
"size"=>60,
"maxlength"=>80,
"minlength"=>1,
"length_e"=>"Please enter the from address field."
));

                $permquery = "SELECT template,template_no FROM template WHERE template_no<>999 ORDER BY 1";
                $sqlarray = FillSQLArrayTwo($permquery);

                $form->add_element(array(
                "type"=>"select",
                "name"=>"field_template_no",
                "value"=>$CurrentRecord->template_no,
                "options"=>$sqlarray));

//if being called from myself, do validation..
if ($Submit) {

    if ($err=$form->validate()) {
        echo $err;
    }
    else {
        // send to SQL
        if (isset($key)) {
            // UPDATE
            if ( ! isset($delete)) {
$DSDEBUG=1;
               $retval=DoUpdate($HTTP_POST_VARS,'newsletteroption','newsletteroption_no',$key);
            }
        }
        if ($retval)
            $form->SaveAndReturn();
        return;
    }
}


//Begin HTML here
$template_no = 999; // use admin template
$systemname = OneSQLValue( "select systemname from newsletteroption where newsletteroption_no = 1" );
DSBeginPage("$systemname Options",0,1,$template_no);
$form->StartForm("$systemname Options");
#$form->DrawField("field_html_signature","Company signature",0,0,0);
$form->DrawField("field_systemname","System Name",1,0,0);
$form->DrawField("field_html_signature","Company Signature:<BR>(appended to<BR>every eMail sent)".DisplayHTMLEditButton("field_html_signature"),1,0,0);
$form->DrawField("field_html_subscribe","Subscription Page:".DisplayHTMLEditButton("field_html_subscribe"),1,0,0);
$form->DrawField("field_from_address","'From' email address:",1,0,0);
$form->DrawField("field_template_no","Template",1,0,0);
$form->EndForm(!isset($key));
PreviousMenu("newslettermenu.php");
DSEndPage(1,0,$template_no);
?>
