<?php
require ("php/prepend.php3");

$lineend="\\r\\n";

$FILES=array("shipping_status.csv","order_detail.csv","order_header.csv");


if (stristr($HTTP_HOST,"lserv")) {
    $FILE_LOCATION="/home/wwwroot/snapin/staging_html/data/";
}
elseif (stristr($HTTP_HOST,"lamp")) {
    $FILE_LOCATION="/var/www/nationalwebbing/public_html/data/";
}
else {
    $FILE_LOCATION="/home/content/31/6093631/html/nationalwebbing/public_html/data/";
}

if (!$cron) {
    DSBeginPage("Import Web Files");
    echo "<form action=$PHP_SELF METHOD=POST>";
    echo "<h3>Import Website Files...</h3>";
    echo "Please be sure that the files that you want to be imported are in the <b>$FILE_LOCATION</b> directory.<BR>
    You need to upload these manually with an FTP program.";
    echo "<br>Currently, the following files exist:<br>";

    for ($i=0;$i<count($FILES);$i++) {
        echo "<br><b>".$FILES[$i]." (".filesize($FILE_LOCATION.$FILES[$i])." bytes) </b><I>Modified: ".date ("F d Y g:i:s A.", filemtime($FILE_LOCATION.$FILES[$i]))."</i>";
    }

    # count records
    echo "<br>";

    for ($i=0;$i<count($TABLES);$i++) {
        $num_recs = OneSQLValue("SELECT count(*) AS c FROM ".$TABLES[$i]);
        echo "<br>The <b>".$TABLES[$i]."</b> table currently has <b>$num_recs</b> records.";

    }


    echo "<BR><br><input type=\"submit\" name=\"cron\" value=\"Import New Files\">";
    echo "</form>";
    DSEndPage();
}



DoQuery("TRUNCATE TABLE shipping_status");
/*
DoQuery( "
    LOAD DATA
    INFILE '$FILE_LOCATION/shipping_status.csv'
    INTO TABLE shipping_status
    FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ','
    LINES TERMINATED BY '$lineend'
    (cust_num,cust_zip,customer_po,order_num,dte_order,dte_ship,order_amount,carrier,carrier_tracking_num,invoice,bill_zip)" );
*/
$csvfile = fopen("$FILE_LOCATION/shipping_status.csv", 'rt');
$i = 0;
while ($csv_array=fgetcsv($csvfile, 1000, ","))
{
    unset($insert_csv);
    $insert_csv['field_cust_num'] = $csv_array[0];
    $insert_csv['field_cust_zip'] = $csv_array[1];
    $insert_csv['field_customer_po'] = $csv_array[2];
    $insert_csv['field_order_num'] = $csv_array[3];
    $insert_csv['field_dte_order'] = $csv_array[4];
    $insert_csv['field_dte_ship'] = $csv_array[5];
    $insert_csv['field_order_amount'] = $csv_array[6];
    $insert_csv['field_carrier'] = $csv_array[7];
    $insert_csv['field_carrier_tracking_num'] = $csv_array[8];
    $insert_csv['field_invoice'] = $csv_array[9];
    $insert_csv['field_bill_zip'] = $csv_array[10];
    DoInsert($insert_csv,"shipping_status");
    $i++;
}
fclose($csvfile);
echo "shipping_status now has ".OneSQLValue("SELECT COUNT(*) from shipping_status")." records.<BR>";
DoQuery("TRUNCATE TABLE order_header");
/*
DoQuery( "
    LOAD DATA
    INFILE '$FILE_LOCATION/order_header.csv'
    INTO TABLE order_header
    FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ','
    LINES TERMINATED BY '$lineend'
    (order_no,order_num,cust_po,dte_order,dte_staged,dte_ship,cust_num,cust_first,cust_last,cust_company,cust_address_1,cust_address_2,cust_city,cust_state,
    cust_zip,cust_country,order_status,total_order_weight,total_order_cartons,total_order_cube,total_order_extended_price,invoice,bill_zip)");
*/
$csvfile = fopen("$FILE_LOCATION/order_header.csv", 'rt');
$i = 0;
while ($csv_array=fgetcsv($csvfile, 1000, ","))
{
    unset($insert_csv);
    $insert_csv['field_order_no'] = $csv_array[0];
    $insert_csv['field_order_num'] = $csv_array[1];
    $insert_csv['field_cust_po'] = $csv_array[2];
    $insert_csv['field_dte_order'] = $csv_array[3];
    $insert_csv['field_dte_staged'] = $csv_array[4];
    $insert_csv['field_dte_ship'] = $csv_array[5];
    $insert_csv['field_cust_num'] = $csv_array[6];
    $insert_csv['field_cust_first'] = $csv_array[7];
    $insert_csv['field_cust_last'] = $csv_array[8];
    $insert_csv['field_cust_company'] = $csv_array[9];
    $insert_csv['field_cust_address_1'] = $csv_array[10];
    $insert_csv['field_cust_address_2'] = $csv_array[11];
    $insert_csv['field_cust_city'] = $csv_array[12];
    $insert_csv['field_cust_state'] = $csv_array[13];
    $insert_csv['field_cust_zip'] = $csv_array[14];
    $insert_csv['field_cust_country'] = $csv_array[15];
    $insert_csv['field_order_status'] = $csv_array[16];
    $insert_csv['field_total_order_weight'] = $csv_array[17];
    $insert_csv['field_total_order_cartons'] = $csv_array[18];
    $insert_csv['field_total_order_cube'] = $csv_array[19];
    $insert_csv['field_total_order_extended_price'] = $csv_array[20];
    $insert_csv['field_invoice'] = $csv_array[21];
    $insert_csv['field_bill_zip'] = $csv_array[22];
    DoInsert($insert_csv,"order_header");
    $i++;
}
fclose($csvfile);

echo "order_header now has ".OneSQLValue("SELECT COUNT(*) from order_header")." records.<BR>";
DoQuery("TRUNCATE TABLE order_detail");
/*
DoQuery( "
    LOAD DATA
    INFILE '$FILE_LOCATION/order_detail.csv'
    INTO TABLE order_detail
    FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ','
    LINES TERMINATED BY '$lineend'
    (xtra,order_num,item_num,item_description,quantity_to_ship,unit_price,extended_price,num_cartons,ext_weight,ext_cube,quantity_ordered)");
*/
$csvfile = fopen("$FILE_LOCATION/order_detail.csv", 'rt');
$i = 0;
while ($csv_array=fgetcsv($csvfile, 1000, ","))
{
    unset($insert_csv);
    $insert_csv['field_xtra'] = $csv_array[0];
    $insert_csv['field_order_num'] = $csv_array[1];
    $insert_csv['field_item_num'] = $csv_array[2];
    $insert_csv['field_item_description'] = $csv_array[3];
    $insert_csv['field_quantity_to_ship'] = $csv_array[4];
    $insert_csv['field_unit_price'] = $csv_array[5];
    $insert_csv['field_extended_price'] = $csv_array[6];
    $insert_csv['field_num_cartons'] = $csv_array[7];
    $insert_csv['field_ext_weight'] = $csv_array[8];
    $insert_csv['field_ext_cube'] = $csv_array[9];
    $insert_csv['field_quantity_ordered'] = $csv_array[10];
    DoInsert($insert_csv,"order_detail");
    $i++;
}
fclose($csvfile);

echo "order_detail now has ".OneSQLValue("SELECT COUNT(*) from order_detail")." records.<BR>";

echo "DONE! - <a href=admin.html>Return to Admin Menu</a><BR>";
?>
