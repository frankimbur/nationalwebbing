<?
# adminsettings.inc;
# settings for the admin menus:


//GRAP A COPY OF THE SITE VARS, STICK THEM HERE
$DSTEXTFGSITE               = $DSTEXTFG;
$DSLINKSITE                 = $DSLINK;
$DSVLINKSITE                = $DSVLINK;
$DSALINKSITE                = $DSALINK;
$DSHEADINGFGSITE            = $DSHEADINGFG;
$DSLABELFGSITE              = $DSLABELFG;
$DSREQUIREDFGSITE           = $DSREQUIREDFG;
$DSCOLLABELFGSITE           = $DSCOLLABELFG;
$DSHEADINGBGSITE            = $DSHEADINGBG;
$DSWINDOWBGSITE             = $DSWINDOWBG;
$DSCOLLABELBGSITE           = $DSCOLLABELBG;
$DSCOLDATABGSITE            = $DSCOLDATABG;
$DSALTCOLDATABGSITE         = $DSALTCOLDATABG;
$DSHEADINGBGSITE            = $DSHEADINGBG;
$DSWINDOWBGSITE             = $DSWINDOWBG;
$DSCOLLABELBGSITE           = $DSCOLLABELBG;
$DSIMAGEBGSITE              = $DSIMAGEBG;
$DSTABLEALIGNSITE           = $DSTABLEALIGN;
$DSTABLEWIDTHSITE           = $DSTABLEWIDTH;
$DSTABLEBORDERSITE          = $DSTABLEBORDER;
$DSTABLECELLSPACINGSITE     = $DSTABLECELLSPACING;
$DSTABLECELLPADDINGSITE     = $DSTABLECELLPADDING;







//NOW OVERWRITE THEM WITH ADMIN SETTINGS
    $DSTEXTFG                   = "BLACK";//standard text color
    $DSPAGEBG                   = "WHITE";//standard page bgd color
    $DSLINK                     = "#660000";//default color of links
    $DSVLINK                     = "#CC6666";//default color of links
    $DSALINK                     = "#6666CC";//default color of links
    $DSHEADINGFG                = "WHITE";//text on bar on top & bottom
    $DSLABELFG                  = "BLACK";//labels 'First Name', etc.
    $DSREQUIREDFG               = "CRIMSON";//labels when required
    $DSCOLLABELFG               = "WHITE";//text - browser columns
    $DSHEADINGBG                = "";//color of bar on top & bottom
    $DSWINDOWBG                 = "GHOSTWHITE";//bgd color of 'window' in entry form
    $DSCOLLABELBG               = "LIGHTBLUE";//bgd color for labels in browser
    $DSCOLDATABG                = "EBEBED";//bgd color for data in browser
    $DSALTCOLDATABG             = "#D0DAED";//bgd color for data in browser
    $DSHEADINGBG                = "#005DA3";//color of bar on top & bottom
    $DSWINDOWBG                 = "GHOSTWHITE";//bgd color of 'window' in entry form
    $DSCOLLABELBG               = "#7998C8";//bgd color for labels in browser
    $DSIMAGEBG                  = ""; //standard background for pages
    $DSTABLEALIGN               = "LEFT";
    $DSTABLEWIDTH               = "100%";
    $DSTABLEBORDER              = "0";
    $DSTABLECELLSPACING         = "0";
    $DSTABLECELLPADDING         = "0";










?>
