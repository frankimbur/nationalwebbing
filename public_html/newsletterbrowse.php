<?php
########################################################################
# Copyright 1999 Desktop Solutions Software, Inc.
# 516-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
require ("prepend.php3");
//  Uncomment for session management, and end with page_close();
//  page_open(
//  array("sess" => "DS_Session",
//  "auth" => "DS_Auth",
//  "perm" => "DS_Perm"));
#########################################################################
# Browse routine for newsletter.nl_newsletter
# generated at Tuesday 20th of March 2001 04:55:35 PM
##############
require "newsletter.inc";
##############
page_open(array("sess" => "DS_Session",
				"auth" => "DS_Auth",
				"perm" => "DS_Perm"));
##############
if (!$perm->have_perm("admin")) {
    return;
}

$t = new DSBrowse;
$db = new DB_Example;
$t->TableWidth=$DSTABLEWIDTH;
$t->Key = 'newsletter_no'; //must match case used in select statement!
$t->classname = 'newsletter';
$t->db = $db;
$t->Columns = "
    title,
    DATE_FORMAT(dte_sent,'%m/%d/%Y %T'),
    CONCAT('<a href=','newslettermenu.php?action=compose&key=',newsletter_no,'>Resend this newsletter</a>'),
    newsletter_no";
$systemname = OneSQLValue( "select systemname from newsletteroption where newsletteroption_no = 1" );
$t->TopHeading= "$systemname Archives";
$t->fields=array("Title","Date Sent","Resend");
$t->HideFrom = count($t->fields);
$t->SearchChoices = array("newsletter"=>$VARS['nl_newsletter'],"title"=>$VARS['nl_title'],"sentdate"=>$VARS['nl_sentdate']);


## Optional settings:
$t->EditUrl = $VARS['nl_archives_admin_edit'];
$t->DeleteUrl = $VARS['nl_archives_admin_edit'];

#$t->LinkColumns = array({$VARS['nl_title']}=>{$VARS['nl_program_name']} . "?action=compose&key=$key");
#$t->ChangeOrder=1;
$t->NoEditing=1;
$t->NoAdding=1;
#$t->NoQuery = 1;
#$t->WhereClause = "first like '%e%'";
#$t->DisplayRows = 10;

$module_template_no = 999;  // use admin template

DSBeginPage("",0,1,$module_template_no);
$t->display();
PreviousMenu("newslettermenu.php");
DSEndPage(1,0,$module_template_no);

?>
