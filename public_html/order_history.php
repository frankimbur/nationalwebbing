<?php
########################################################################
# Copyright 1999 Desktop Solutions Software, Inc.
# 516-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
require ("prepend.php3");
page_open(
array("sess" => "DS_Session",
"auth" => "DS_Auth",
"perm" => "DS_Perm"));
if (!HasPermissions("admin")) {
    return;
}
$systemname = OneSQLValue( "select systemname from orderhistoryoption where orderhistoryoption_no = 1" );
$module_template_no = 999;  // use admin template
DSBeginPage("$systemname Menu",0,1,$module_template_no);
StartDCMenu("$systemname Menu");
DrawDCMenuLine("$systemname Actions");
DrawDCMenuLine("Maintain Items","order_header_browse.dtop");
DrawDCMenuLine("Manual Data Upload (header)","importcsv.dtop?tablename=order_header");
DrawDCMenuLine("Manual Data Upload (detail)","importcsv.dtop?tablename=order_detail");
DrawDCMenuLine("View Lookup Page","order_history.html");
DrawDCMenuLine("Program Options","orderhistoryoptions.dtop");
DrawDCMenuLine("Other Options");
DrawDCMenuLine("View $systemname documentation",'http://www.desktopmodules.com/docs/pastorders.pdf');
DrawDCMenuLine("Return to main menu",'admin.html');
EndDCMenu();

DSEndPage(1,0,$module_template_no);
?>
