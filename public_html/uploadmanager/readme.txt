QTOFileManager 1.0
http://www.qto.com/fm
Author: Quentin O'Sullivan
E-mail: quentin@qto.com


INSTALLATION

1. Put the php file and the 3 gif images in a directory

2. You may want to change the configuration variables in the top 
of the program but this is optional, you can just use the default settings.


WHAT IT DOES
- Upload and delete files from the directory (folder) the program is in and any sub directories.

- It can authenticate the user (optional)

- It allows you to edit a file (optional)

- Specify which file extensions can be edited

- Add and remove directories (optional)

- Specify the max hard drive space allowed

- Specify the max file size allowed

- You can designate file names as being hidden and they will then not show
up in the file manager e.g. .htaccess

- The program will strip out any % characters from a file name because these 
cause problems.

- When the program calculates the total size of the files it adds up the size of all the files 
including the hidden ones.


AUTHENTICATION
If you want authentication you must set the $user and $password configuration 
variable in the program. If you don't want authentication set $user = ""
Please note the authentication system is not bullet proof, it could be broken into
if someone was determined enough


MAX FILE SIZE
The program sets a limit for the maximum allowed file size. The default is 1MB. You 
can change this by setting the $MaxFileSize configuration variable in the program.


MAX HDD SPACE
The program sets a limit for the maximum total size of all files in the directory. The 
default is 100MB. You can change this by setting the $HDDSpace configuration variable 
in the program.


HIDDEN FILES
The program allows for hidden files. You can designate a file as a hidden file by adding 
the file name to the $HiddenFiles array in the program. 
e.g. $HiddenFiles = array("file1.ext","file2.jpg","file3.jpg");


EDITING FILES
The program by default allows you to edit files with the extensions .htm .html .txt
If you want to edit files with other extensions you need to change the $EditExtensions
configuration variable in the top of the program. Just add or remove extensions from 
the $EditExtensions array. You can also turn file editing off altogether by setting
$EditOn = 0 


ADD AND REMOVE DIRECTORIES (FOLDERS)
The program allows you to add and delete directories. This functionality can be turned
off by setting $MakeDirOn = 0 in the configuration variables at the top of the program


SECURITY
The authentication system is not bullet proof, it could be broken into
if someone was determined enough. Do not use this program in directories that 
contain important files such as credit card numbers.


HELP and BUGS
If you find any bugs or you need some help get in touch and I might be able to help/

Quentin O'Sullivan
E-mail: quentin@qto.com
Web Site: http://www.qto.com


CUSTOMISING
The HTML page is at the very bottom of the program file you should be able to easily
customise this for your web site by either hand coding the HTML or using an editor
such as Dreamweaver.

The HTML that makes up the file manager itself is in the program code and you may 
require some knowledge of PHP to alter this although you should be able to change 
the fonts with only HTML knowledge. The look of the file manager has been designed 
to be very neutral so that it can easily slot into an existing web site without clashing
with the existing colors.
 

LICENSE
This program is distributed under a GNU General Public License (GPL). Please read 
the GNU License file for more information about how you can distribute the program.


