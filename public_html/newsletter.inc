<?
##############
# newsletter.inc - include file for newsletter software
# Tom Melendez
##############
# script names
# Merge fields - in the future, ability for users to add their own
## user
$NL_FIRSTNAME                   = "#FIRSTNAME#";
$NL_FIRSTNAME_MAP               = "first";
$NL_LASTNAME                    = "#LASTNAME#";
$NL_LASTNAME_MAP                = "last";
$NL_EMAILADDRESS                = "#EMAILADDRESS#";
$NL_EMAILADDRESS_MAP            = "username";

##############
Function PreviewEmail()
##############
{
    global $HTTP_POST_VARS,$DSDEBUG,$_SERVER;
#   $DSDEBUG=1;
    # first save any changes the user made
    DoUpdate($HTTP_POST_VARS,'newsletteroption','newsletteroption_no',1);
    $Options=ReadCurrentRecord('newsletteroption','newsletteroption_no',1);
    echo '<base HREF="HTTP://'.$_SERVER['HTTP_HOST'].'">';
    echo "<HR 100%>";
    echo stripslashes($Options->html_message);
    echo stripslashes($Options->signature);
    echo "<HR 100%>";
    echo "<a href=newslettermenu.php?action=compose>Return to composing your newsletter</A>";
}
#############
# Function ValidateOwnerTable($action="print")
# checks to see if all required info is in the owner table
# if action = print, it will print a message to the screen
# if action = exit, the message will be printed and the program will terminate
#############
Function ValidateOwnerTable($action="print")
{
    global $PHP_SELF,$HTTP_POST_VARS,$VARS,$DSDEBUG;
    $error=0;
    $Owner = ReadCurrentRecord(newsletteroption,newsletteroption_no,1);
   	if (!strlen($Owner->from_address)) {
       echo "<font color=#FF0000>From Address is blank in newsletteroption table";
       $error=1;
    }
    if (strtoupper($action) == "EXIT" && $error==1) {
       exit();
    }
}

#############
# Function CheckForUserInDB($email)
# checks for user in the db
# returns key if found, else nothing
#############
Function CheckForUserInDB($email)
{
      global $PHP_SELF,$HTTP_POST_VARS,$VARS;
      $q = "SELECT uid as k FROM mailinglist WHERE username = '$email'";
      $retval = DoQuery($q);
      if (mysql_num_rows($retval)>0) {
         return 1;
      }
      else {
         return 0;
      }
}
#############
# Function GetMemberCount()
# returns the number of subscribers to the list
#############
Function GetMemberCount()
{
    global $DSDEBUG;
    return OneSQLValue("SELECT count(*) FROM mailinglist WHERE receiveupdates='Y' AND active='Y'");
}
#############
# Function ComposeEmail()
# draws the screen to compose an email
# also initiates a check for new adds and deletes
#############
Function ComposeEmail()
{
    global $VARS,$key,$PHP_SELF,$form,$DSHEADINGBG,$DSLABELFG;
    ValidateOwnerTable();
    if ($key)
        $CurrentRecord = ReadCurrentRecord(newsletter,newsletter_no,$key);
    else
        $CurrentRecord = ReadCurrentRecord(newsletteroption,newsletteroption_no,1);
    $UsersRecord=GetUsersRecord();
    $form = new DSForm;

    $form->add_element(array(
    "type"=>"text",
    "name"=>"testemailaddress",
    "value"=>$UsersRecord->username,
    "size"=>60,
    "minlength"=>1,
    "length_e"=>"Please enter your email address."
    ));

    $options  = FilterArray('N');
    $form->add_element(array(
    "type"=>"select",
    "name"=>"field_filter_no",
    "value"=>$CurrentRecord->filter_no,
    "options"=>$options));

    $form->add_element(array(
    "type"=>"text",
    "name"=>"field_title",
    "value"=>$CurrentRecord->title,
    "size"=>60,
    "minlength"=>1,
    "length_e"=>"Please enter the title."
    ));

    $form->add_element(array(
    "type"=>"textarea",
    "name"=>"field_html_message",
    "value"=>$CurrentRecord->html_message,
    "cols"=>70,
    "rows"=>20
    ));

    $form->add_element(array(
    "type"=>"checkbox",
    "name"=>"testemail",
    "checked"=>1,
    "cols"=>70,
    "rows"=>20
    ));

    $form->add_element(array(
    "type"=>"hidden",
    "name"=>"action",
    "value"=>"sendemail"
    ));

    $form->StartForm("Send eMail to List<BR>".GetMemberCount() . " members on this list");
    $form->DrawField("testemailaddress","Your e-mail:",1,0,0);
    $form->DrawField("field_filter_no","Filter:",1,0,0);
    $form->DrawField("field_title","Subject:",1,0,0);
    $form->DrawField("testemail","Send test only?",1,0,0);
    $form->DrawField("field_html_message","Your Message:".DisplayHTMLEditButton("field_html_message").DisplayPureHTMLButton("pure_html_message"),1,0,0);
#   $form->EndForm(!isset($key));
    echo "<TR><TD COLSPAN=2 BGCOLOR=$DSHEADINGBG ALIGN=RIGHT>";
    $form->show_element("Submit","Preview");
    $form->show_element("Submit","Submit");
    echo "</TR></TD>";
    $form->finish($jsafter,$jsbefore);
    echo "</table>";
    PreviousMenu("newslettermenu.php");
}

############
# Function ValidateEmail($email)
# checks to see if an email is valid
# returns true or false
############
Function ValidateEmail($email)
{
    $status = false;
    if (strlen($email)>=5) {
        if (strstr($email,"@")) {
            if (strstr($email,".")) {
                $status = true;
            }
        }
    }
    return $status;
}

#############
# Function InsertMessageIntoArchive
# inserts the message into the archive
# returns the key on success
#############
Function InsertMessageIntoArchive($message,$subject)
{
    global $PHP_SELF,$HTTP_POST_VARS,$VARS;
    $i = "INSERT INTO newsletter (html_message,title,dte_sent) VALUES('$message','$subject',Now())";
    $retval = DoQuery($i);
    return mysql_insert_id();

}

#############
# Function SendEmail
# sends the email
#############
Function SendNewsletter()
{
    global $PHP_SELF,$HTTP_POST_VARS,$HTTP_POST_FILES,$VARS, $key, $HOW_TO_SEND_MAIL,$MAIL_PORT,
    $SLEEP_AFTER_MAILS, $TIME_TO_SLEEP_FOR,$DSDEBUG;
    # validate everything first
    ValidateOwnerTable("exit");
    print (
    "Sending Mail...Please Wait...<BR>
    <B>DO NOT PRESS BACK BUTTON OR CLOSE BROWSER, THIS WILL TAKE A FEW MINUTES...</B>");
    flush();

    $Owner = ReadCurrentRecord(newsletteroption,newsletteroption_no,1);
    $Options=ReadCurrentRecord('newsletteroption','newsletteroption_no',1);
    $Filter =ReadCurrentRecord('filter','filter_no',$Options->filter_no);

    $phpversion=floor(phpversion());
    
    $testemail = isset($HTTP_POST_VARS['testemail']);
    
    if (!$testemail) {
        if (strlen($Filter->sql))
            $q = "SELECT * FROM mailinglist " . $Filter->sql." AND receiveupdates='Y' AND active='Y'";
        else
            $q = "SELECT * FROM mailinglist WHERE receiveupdates='Y' AND active='Y'";
    }
    else {
        $q = "SELECT '".$HTTP_POST_VARS['testemailaddress']."' AS username";
    }
    $retval = DoQuery($q);
    flush();
    $headers = "From: ". $Owner->from_address. "\r\n";
    $headers .= "Content-Type: text/html; charset=iso-8859-1\n";

    while ($row = mysql_fetch_object($retval)) {
        if( strlen( $HTTP_POST_VARS['pure_html_message'] ) > 0 )
        {
          $message = stripslashes(MergeNLFields($HTTP_POST_VARS['pure_html_message'] ."\n\n".$Owner->html_signature,1,$row->uid));
        }
        else
        {
          $message = stripslashes(MergeNLFields($HTTP_POST_VARS['field_html_message'] ."\n\n".$Owner->html_signature,1,$row->uid));
        }
        
        $poweredby_signature = <<<END
&nbsp;<br><p><a href="http://www.desktopsolutions.com/modules.php"><img src="http://www.desktopsolutions.com/images/newslettericon.gif" border="0" alt="Powered by Desktop Solutions"><br>Powered by Desktop Solutions</a></p>
END;
        
        $message .= $poweredby_signature;

        mail($row->username,
             stripslashes($HTTP_POST_VARS['field_title']),
             $message,
             $headers
             );
        $counter++;
        if ($SLEEP_AFTER_MAILS > 0) {
            if ($counter % $SLEEP_AFTER_MAILS == 0) {
                sleep($TIME_TO_SLEEP_FOR);
            }
        }
    }

    if ($HTTP_POST_VARS['testemail']) {
        echo "(single message test mode)... ";
    }
    echo "done...$counter messages sent";
    if (!$testmail) {
        $m = "Your newsletter has been sent on ".date("m/d/Y"). " to $counter subscribers.\n";
        mail($HTTP_POST_VARS['testemailaddress'],
        "Your Newsletter has been sent",
        $m,
        $headers
        );
        DoInsert($HTTP_POST_VARS,'newsletter','newsletter_no',1);
        DoUpdate($HTTP_POST_VARS,'newsletteroption','newsletteroption_no',1);
    }
    else {
        $HTTP_POST_VARS['field_html_message']='';
        $HTTP_POST_VARS['field_title']='';
        DoUpdate($HTTP_POST_VARS,'newsletteroption','newsletteroption_no',1);
    }
    echo "<BR><a href=newslettermenu.php>Back to Newsletter Menu</A>";
}

###########
Function MergeNLFields($text,$ownerkey=1,$userkey)
###########
{
    global $VARS,$testemail;
    global $NL_FIRSTNAME,$NL_FIRSTNAME_MAP,$NL_LASTNAME,$NL_LASTNAME_MAP,
    $NL_NAME,$NL_NAME_MAP,$NL_ADDRESS1,$NL_ADDRESS1_MAP,$NL_ADDRESS2,
    $NL_ADDRESS2_MAP,$NL_CITY,$NL_CITY_MAP,$NL_STATE,$NL_STATE_MAP,
    $NL_ZIP,$NL_ZIP_MAP,$NL_EMAILADDRESS,$NL_EMAILADDRESS_MAP,
    $NL_COMPANYNAME,$NL_COMPANYNAME_MAP,$NL_COMPANYADDRESS1,
    $NL_COMPANYADDRESS1_MAP,$NL_COMPANYADDRESS2,$NL_COMPANYADDRESS2_MAP,
    $NL_COMPANYCITY,$NL_COMPANYCITY_MAP,$NL_COMPANYSTATE,$NL_COMPANYSTATE_MAP,
    $NL_COMPANYZIP,$NL_COMPANYZIP_MAP,$NL_COMPANYPHONE,$NL_COMPANYPHONE_MAP,
    $NL_COMPANYFAX,$NL_COMPANYFAX_MAP,$NL_COMPANYURL,$NL_COMPANYURL_MAP,
    $NL_COMPANYSUBSCRIBE,$NL_COMPANYSUBSCRIBE_MAP,$NL_COMPANYUNSUBSCRIBE,
    $NL_SUBSCRIPTION_PAGE,$NL_SUBSCRIPTION_PAGE_MAP;


    if (!$testemail) { //don't execute if is a test email
        $User = ReadCurrentRecord(mailinglist,uid,$userkey);
        $Owner = ReadCurrentRecord(newsletteroption,newsletteroption_no,$ownerkey);


        ## user
        $text = ereg_replace($NL_FIRSTNAME, $User->$NL_FIRSTNAME_MAP,$text);
        $text = ereg_replace($NL_LASTNAME,$User->$NL_LASTNAME_MAP,$text);
        $text = ereg_replace($NL_EMAILADDRESS,$User->$NL_EMAILADDRESS_MAP,$text);

    }
    return $text;
}
