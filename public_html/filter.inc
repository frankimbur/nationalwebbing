<?
##############
# filter.inc - include file for filter system
##############

################
Function CheckFilterValid($filter)
################
{
    $msg=FilterError($filter);
    if ($msg) {
        echo "There is an error in the filter:$filter<BR>";
        echo "$msg";
        return 0;
    }
    else {
        return 1;
    }
}


##############
Function FilterError($filter)
##############
{
    global $DSDEBUG,$DSDATABASE,$DSHOST,$DSUSER,$DSPASSWORD;
    if ($DSDEBUG)
        echo "DEBUG:<BR>".$filter."<BR>";

    mysql_connect($DSHOST,$DSUSER,$DSPASSWORD);
    $result=@mysql_db_query($DSDATABASE,$filter);
    if (! $result)
        return mysql_error();
    else
        return "";
}
############
function BuildFilter($filter_no)
############
{
    global $DSDEBUG;
    $results=DoQuery("SELECT * from filterfield  WHERE filter_no=$filter_no");
    while ($CurrentRecord=mysql_fetch_object($results)) {
        if (strlen($sql))
            $sql.=' AND ';
        $sql.=$CurrentRecord->fieldname;
        switch($CurrentRecord->matchtype){
            case 'equals':
                $sql.=' = "'.addslashes($CurrentRecord->data).'"';
                break;
            case 'is not':
                $sql.=' != "'.addslashes($CurrentRecord->data).'"';
                break;
            case 'ends with':
                $sql.=' LIKE "%'.addslashes($CurrentRecord->data).'"';
                break;
            case 'begins with':
                $sql.=' LIKE "'.addslashes($CurrentRecord->data).'%"';
                break;
            case 'contains':
                $sql.=' LIKE "%'.addslashes($CurrentRecord->data).'%"';
                break;
            case 'is in list of':
                $data=str_replace(',','","',$CurrentRecord->data);
                $sql.=' IN("'.addslashes($data).'")';
                break;
            case 'less than':
                $sql.=' < "'.addslashes($CurrentRecord->data).'"';
                break;
            case 'greater than':
                $sql.=' > "'.addslashes($CurrentRecord->data).'"';
                break;
        }
    }
    return "WHERE $sql";
}
//***********
function FilterArray($type)
//***********
{
    $result = DoQuery("SELECT filter,filter_no FROM filter WHERE type='$type' ORDER BY filter");
    $loops=0;
    $retarray[$loops]["label"]="-- None --";
    $retarray[$loops++]["value"]="0";
    if ($result) {
        while($row = mysql_fetch_array($result)) {
              # TJM 12/19/2003 - add receipients count to each
              $filtersql=OneSQLValue("SELECT sql FROM filter WHERE filter_no=".$row[1]);
              $retarray[$loops]["label"]=$row[0]." (".OneSQLValue("SELECT count(*) FROM mailinglist $filtersql AND active = 'Y' AND receiveupdates = 'Y'"). " addresses)";
              $retarray[$loops++]["value"]=$row[1];
        }
        mysql_free_result($result);
        return $retarray;
    }
    return Array("");
}


?>
