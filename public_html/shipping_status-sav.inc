<?php
########################################################################
# Copyright 2002 Desktop Solutions Software, Inc.
# 631-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
#include for shipping tracking module...
##########
Function ShippingStatusTrackingUrl($carrier_tracking_num,$carrier)
##########
# returns a complete url to the tracking site
{
    $carrier_link="&nbsp;";

    if (strlen(trim($carrier_tracking_num))<2)
        return $carrier_link;

    switch($carrier) {
        case "UPS":
            $carrier_link="<a href=\"http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=1&TypeOfInquiryNumber=T&HTMLVersion=4.0&InquiryNumber1=".$carrier_tracking_num."&track=Track\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "FEDEX":
            $carrier_link="<a href=\"http://www.fedex.com/cgi-bin/tracking?tracknumbers=".$carrier_tracking_num."&language=english&action=track&cntry_code=us\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "ROADWAY":
            $carrier_link="<a href=\"http://www.quiktrak.roadway.com/cgi-bin/quiktrak?type=0&pro0=".$carrier_tracking_num."&zip0=&pro1=&zip1=&pro2=&zip2=&pro3=&zip3=&pro4=&zip4=&pro5=&zip5=&pro6=&zip6=&pro7=&zip7=&pro8=&zip8=&pro9=&zip9=&auth=0qmsUAkRe7M&submit.x=6&submit.y=22\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "BAX":
            $carrier_link = "<a href=\"http://www.baxglobal.com/win-cgi/cwstrack.dll?trackby=H&trackbyno=".$carrier_tracking_num."&org=&dst=&mnth1=&day1=&year1=&mnth2=&day2=&year2=&submit1.x=14&submit1.y=18\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "NEWPENN":
            $carrier_link = "<a href=\"http://www.newpenn.com/npweb/tracking.txt/process?p_input_typ=1&p_trak_1=".$carrier_tracking_num."\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "ABF":
            $carrier_link = "<a href=\"http://www.abfs.com/trace/abftrace.asp?blnBOL=TRUE&blnPO=TRUE&blnShipper=TRUE&blnConsignee=TRUE&blnABFGraphic=TRUE&blnOrigin=TRUE&blnDestination=TRUE&RefType=a&bhcp=1&Ref=".$carrier_tracking_num."\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "REDSTAR":
            $carrier_link="<a href=\"http://www.usfc.com/tools/truckingresultsdetail.asp?txtLookupNumber=$carrier_tracking_num&radLookupNumberType=H&SearchType=1\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "YELLOW":
            $carrier_link = "<a href=\"http://www2.yellowcorp.com/dynamic/services/servlet?diff=protrace&CONTROLLER=com.yell.ec.inter.yfsgentracking.http.controller.TrackPro&DESTINATION=%2Fyfsgentracking%2Ftrackingresults.jsp&SOURCE=%2Fyfsgentracking%2Ftrackpro.jsp&FBNUM2=&FBNUM3=&FBNUM4=&FBNUM5=&FBNUM1=".$carrier_tracking_num."\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "DHL":
            $carrier_link="<a href=\"http://www.dhl.com/cgi-bin/tracking.pl?AWB=".$carrier_tracking_num."&TID=CP_ENG\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "FEDEXEAST":
            $carrier_link="<a href=\"http://www.fedexfreight.fedex.com/protrace.jsp?as_type=PRO&as_pro=".$carrier_tracking_num."\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "EMERY":
            $carrier_link="<a href=\"http://www.emeryworld.com/tracking/trackformaction.asp?optTYPE=SHIPNUM&PRO1=".$carrier_tracking_num."\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "GOD":
            $carrier_link="<a href=\"http://www.1800dialgod.com/quickpro.asp?cat=search&Prono=".$carrier_tracking_num."\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        case "OLDDOMINION":  //Old Dominion
            $carrier_link="<a href=\"http://www.odfl.com/trace/Trace.jsp?Type=P&pronum=".$carrier_tracking_num."\" target=\"_blank\">$carrier_tracking_num</a>";
            break;
        }
    return $carrier_link;
}
?>
