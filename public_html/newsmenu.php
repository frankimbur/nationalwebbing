<?php
########################################################################
# Copyright 1999 Desktop Solutions Software, Inc.
# 516-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
require ("prepend.php3");
page_open(
array("sess" => "DS_Session",
"auth" => "DS_Auth",
"perm" => "DS_Perm"));
if (!HasPermissions("admin")) {
    return;
}
#$DSDEBUG=1;
#$heading=NewsField("news_heading");
$module_template_no = 999;  // use admin template
$systemname = OneSQLValue( "select systemname from newsoption where newsoption_no = 1" );
DSBeginPage("$systemname Menu",0,1,$module_template_no);
StartDCMenu("$systemname Menu");
## Display products
DrawDCMenuLine("$systemname Actions");
## DrawDCMenuLine("$heading Menu");
DrawDCMenuLine("Maintain Items","news_browse.dtop");
DrawDCMenuLine("Program Options","newsoptions.dtop");
DrawDCMenuLine("View $systemname page","news.html");
DrawDCMenuLine("Maintain Categories","newscategory_browse.dtop");
DrawDCMenuLine("Maintain Types","newstype_browse.dtop");
DrawDCMenuLine("$systemname Teaser Content","newsteaseroptions.dtop");
DrawDCMenuLine("Other Options");
DrawDCMenuLine("View $systemname Documentation","http://www.desktopmodules.com/docs/headlines.pdf");
DrawDCMenuLine("Return to main menu",'admin.html');
EndDCMenu();

DSEndPage(1,0,$module_template_no); ##CET removed skip extra and added template
?>
