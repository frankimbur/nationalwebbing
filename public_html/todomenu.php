<?php
########################################################################
# Copyright 2003 Desktop Solutions Software, Inc.
# 631-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
require ("prepend.php3");
page_open(
array("sess" => "DS_Session",
"auth" => "DS_Auth",
"perm" => "DS_Perm"));
if (!HasPermissions("admin")) {
    return;
}
$module_template_no = 999;  // use admin template
$systemname = OneSQLValue( "select systemname from todooption where todooption_no = 1" );
DSBeginPage("$systemname Menu",0,1,$module_template_no);
StartDCMenu("$systemname Menu");

#
# Module Sub-Menus
#

if (defined("TODO")){
    DrawDCMenuLine("$systemname Actions");
    DrawDCMenuLine("Add an item","todo_edit.dtop");
    DrawDCMenuLine("Maintain Items","todo_browse.dtop?status=active");
    DrawDCMenuLine("Report","todoreportoptions.html?goto=todoreport.dtop");
    DrawDCMenuLine("Maintain Categories","todocategory_browse.dtop");
    DrawDCMenuLine("Maintain Classes","todoclass_browse.dtop");
    DrawDCMenuLine("Program Options",'todooption_edit.php');
}
DrawDCMenuLine("Other Options");
DrawDCMenuLine("View $systemname Documentation","http://www.desktopmodules.com/docs/todolist.pdf");
DrawDCMenuLine("Return to main menu",'admin.html');
EndDCMenu();

DSEndPage(1,0,$module_template_no); ##CET removed skip extra and added template
?>
