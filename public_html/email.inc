<?php
########################################################################
# Copyright 2001 Desktop Solutions Software, Inc.
# 631-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
# email.inc - email engine file
# $Id: email.inc,v 1.1.1.1 2001/11/28 20:11:54 tom Exp $
# this file will call SendEmail($message,$to,$subject="",$from="") from project.inc

############
# Function SalesEmail($userid,$list_no,$subject="",$to="COMPANY")
# sends email to company employees stating an order has been placed on the website.
# $to - values:
# CUSTOMER sends confirmation to CUSTOMER
# COMPANY sends request to COMPANY
# PRICED sends priced email to CUSTOMER
# ACCEPT sends acceptance email to COMPANY
# AUTHORIZE sends email to authorizer
# RECEIPT sends receipt email to CUSTOMER
# everything else to ADMINISTRATOR
############
Function SalesEmail($userid,$list_no,$subject="",$to="COMPANY")
{
    global $DSSYSTEMNAME,$DSCARTNAME;
    if (!$subject) {
        $subject = "$DSCARTNAME from $DSSYSTEMNAME";
    }
    if (strtoupper($to)=="COMPANY") {
        $to = SiteField("sales_emailid");
        $textfield = "email_to_company";
    }
    else
        if (strtoupper($to)=="CUSTOMER") {
            $to = OneSQLValue("SELECT username FROM mailinglist WHERE uid = $userid");
            $textfield = "email_to_customer";
        }
    else
        if (strtoupper($to)=="PRICED") {
            $to = OneSQLValue("SELECT username FROM mailinglist WHERE uid = $userid");
            $textfield = "price_email_to_customer";
        }
    else
        if (strtoupper($to)=="ACCEPT") {
            $to = SiteField("sales_emailid");
            $textfield = "accept_email_to_company";
        }
    else
        if (strtoupper($to)=="AUTHORIZE") {
            $to = OneSQLValue("SELECT authorizeby FROM mailinglist WHERE uid = $userid");
            $textfield = "authorize_email";
        }
    else
        if (strtoupper($to)=="RECEIPT") {
            $to = OneSQLValue("SELECT username FROM mailinglist WHERE uid = $userid");
            $textfield = "receipt_email_to_customer";
        }
    else {
        $to = SiteField("administrator_emailid");
        $textfield = "email_to_company";
    }
    $text = SiteField($textfield);
    $message = MergeFields($text,$userid,$list_no);
    $message .= GetListContents($list_no);
    SendEmail($message,$to,$subject);

}

###########
# Function GetListContents($list_no)
# gets the contents of list for email
# showprices: 1 for yes, 0 for no
# DOESN'T DO quantityxitem MATH, SHOULD IT?
# does do sum of price
###########
Function GetListContents($list_no)
{
    $cartcontents="";
    $separator="";
    $cartheader="";
    $total = 0;
    $showprices=SiteField("enterprices")=="Y";

    $separator.=str_repeat("=",65)."\n";
    $ListRec = ReadCurrentRecord("list","list_no",$list_no);
    if ($ListRec->list_no>0) {
        $rvdetails = DoQuery("SELECT * FROM listdetail WHERE list_no=$list_no");
        if (mysql_num_rows($rvdetails)>0) {
            while ($row = mysql_fetch_object($rvdetails)) {
                $cartcontents.= sprintf("%-15s",$row->itemnum);
                $cartcontents.= sprintf("%-35s",substr($row->description,0,32));
                $cartcontents.= sprintf("%-10s",$row->quantity);
                if ($showprices) {
                    $cartcontents.= sprintf("%-15s",$row->discountprice);
                }
                $cartcontents.="\n";
            }
            $cartheader.=sprintf("%-15s%-35s%-10s","Item #","Description","Quantity");
            if ($showprices) {
                $total  = number_format($total,2);
                $cartheader.=sprintf("%-10s","Price");
                $cartfooter.=  "Item Total :$$ListRec->disctotal";
                $cartfooter.="\nTax        :$$ListRec->tax_amount";
                $cartfooter.="\nShipping   :$$ListRec->freight_amount";
                $cartfooter.="\nGrand Total:$$ListRec->grandtotal\n";
                $cartfooter.=$separator;
            }
            $cartheader.="\n";

            return "\n".$separator.$cartheader.$separator.$cartcontents.$separator.$cartfooter;
        }
        else {
            return "*no items found in list*";
        }
    }
    else {
       return "*no list found*";
    }
}

######
# Function GetCartContentsForEmail($cartid="")
# this function needs to be changed to work with the system
# in place of the one above
######
Function GetCartContentsForEmail($cartid="")
{
    if (!strlen($cartid)) {
        return "";
    }

    $CartRec = ReadCurrentRecord("list","list_no",$cartid);
    # this query will pick the discount price if it is NOT 0, else take the price
    $retval = DoQuery("SELECT listdetail.model_no,quantity,IF (discprice<>0,discprice,price) as itemprice,itemcomment,model.itdsc
                        FROM listdetail,model WHERE model.model_no = listdetail.model_no AND list_no=$cartid");

    $space_char = " ";
    $MAX_LENGTH=25;

    # set up field lengths and headings
    $length[model_no] = mysql_field_len($retval,0) + 1;
    $length[quantity] = mysql_field_len($retval,1) + 1;
    $length[itemprice]= mysql_field_len($retval,2) + 1;;
    # this is a text field
    $length[itemdescription]= mysql_field_len($retval,3);

    $length[itdsc]=$MAX_LENGTH;

    $heading[quantity] = "Qty";
    $heading[model_no] = "Item Number";
    $heading[itemprice] = "Each";
    $heading[itdsc]="Item Description";
    $heading[subtotal] = "Subtotal";
    # totals
    $heading[mer_subtotal] = "MERCHANDISE SUBTOTAL:";
    $heading[shipping] = "SHIPPING CHARGES:";
    $heading[grandtotal] = "GRAND TOTAL:";
    $heading[tax] = "SALES TAX (IF APPLICABLE):";


    # build header
    $headingline.= $heading[quantity]. @str_repeat($space_char,$length[quantity]-(strlen($heading[quantity]))) . $heading[model_no].@str_repeat($space_char,$length[model_no]-(strlen($heading[model_no])));
    $headingline.=$heading[itdsc] . @str_repeat($space_char,$length[itdsc]-(strlen($heading[itdsc])));
    $headingline.= @str_repeat($space_char,$length[itemprice]-(strlen($heading[itemprice]))) . $heading[itemprice];
    $headingline.= @str_repeat($space_char,$length[quantity]-(strlen($heading[subtotal]))+1). $heading[subtotal];
    $headingline.="\n";

    while ($row = mysql_fetch_object($retval)) {

        $oneline="";
        $oneline.=$row->quantity . @str_repeat($space_char,$length[quantity]-(strlen($row->quantity))) . $row->model_no .@str_repeat($space_char,$length[model_no]-(strlen($row->model_no)));
        $oneline.=substr($row->itdsc,0,$MAX_LENGTH) . @str_repeat($space_char,$length[itdsc]-(strlen($row->itdsc)));
        $oneline.=@str_repeat($space_char,$length[itemprice]-(strlen($row->itemprice))). $row->itemprice . @str_repeat($space_char,1);
        $subtotal = $row->itemprice * $row->quantity;
        $subtotalline = number_format($subtotal,2);
        $oneline.=@str_repeat($space_char,$length[itemprice]-(strlen($subtotalline))) . $subtotalline;
        $lineitems.= $oneline . "\n";
        $linelength=strlen($oneline);
        if (strlen(trim($row->itemcomment))>1) {
            $spacer = $length[quantity]+$length[model_no];
#            $lineitems.= wordwrap(trim("Comments: ".$row->itemcomment),$linelength,"\n") . "\n";
            $lineitems.= wordwrap(str_repeat($space_char,$spacer).trim("Comments: ".$row->itemcomment),$linelength-$spacer,"\n".str_repeat($space_char,$spacer)) . "\n";
        }
        $total+=$subtotal;


    }


    if ($CartRec->total>0) {
        (strlen($CartRec->disctotal))?$subtotalprice=$CartRec->disctotal:$CartRec->total;
        $totalline.=$heading[mer_subtotal]. @str_repeat($space_char,$linelength-( strlen(number_format($total,2)) + strlen($heading[mer_subtotal]) )).number_format($total,2). "\n";
    }

    if ($CartRec->freight_amount>0) {
        $freightline.=$heading[shipping]. @str_repeat($space_char,$linelength-( strlen(number_format($CartRec->freight_amount,2)) + strlen($heading[shipping]) )).number_format($CartRec->freight_amount,2). "\n";
    }

    if ($CartRec->tax_amount>0) {
        $taxline.=$heading[tax]. @str_repeat($space_char,$linelength-( strlen(number_format($CartRec->tax_amount,2)) + strlen($heading[tax]) )).number_format($CartRec->tax_amount,2). "\n";

    }

    if ($CartRec->grandtotal>0) {
        $grandtotalline.=$heading[grandtotal]. @str_repeat($space_char,$linelength-( strlen("$".number_format($CartRec->grandtotal,2)) + strlen($heading[grandtotal]) ))."$".number_format($CartRec->grandtotal,2). "\n";
    }

    $line = str_repeat("-",$linelength) . "\n";
    $string = $line.$headingline.$line.$lineitems.$line.$totalline.$freightline.$taxline.$grandtotalline.$footer;
    return $string;
}




?>
