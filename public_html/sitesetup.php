<?php
########################################################################
# Copyright 2002 Desktop Solutions Software, Inc.
# 631-493-3422
# info@desktopsolutions.com
# www.desktopsolutions.com
# usage rights granted for use on a per-project-basis
#########################################################################
require ("prepend.php3");
//  Uncomment for session management, and end with page_close();
page_open(
array("sess" => "DS_Session",
"auth" => "DS_Auth",
"perm" => "DS_Perm"));
if (!HasPermissions("admin")) {
    return;
}
$form = new DSForm;

$key=1; //hard coded for edit...

## Optional settings:
#$form->AddAnother=1;
#$form->NextPrev='fieldname';
if (!strlen($referer))
    $referer=$HTTP_REFERER;
$form->add_element(array(
                    "type"=>"hidden",
                    "name"=>"tablename",
                    "value"=>$tablename
                    ));
$form->add_element(array(
                    "type"=>"hidden",
                    "name"=>"referer",
                    "value"=>$referer
                    ));

$CurrentRecord = ReadCurrentRecord("sitesetup","site_no",$key);
if (isset($do_over)) {
    // we've been called from ourselves, second time in a row...
    // the key should have been passed in, causing defaults to be loaded above
    // now when we unset , the rest of the script will be in 'add' mode..
    unset($key);
}
//Set up fields here
if (isset($key)) {
    $form->add_element(array(
                        "type"=>"hidden",
                        "name"=>"key",
                        "value"=>$key
                        ));
}
if (isset($delete)) {
    $form->add_element(array(
                        "type"=>"hidden",
                        "name"=>"delete",
                        "value"=>1
                        ));
}
if (isset($returnto)) {
    $form->add_element(array(
                        "type"=>"hidden",
                        "name"=>"returnto",
                        "value"=>$returnto
                        ));
}

            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_site_no",
            "value"=>$CurrentRecord->site_no,
            "size"=>10
                ,"maxlength"=>10
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Site no field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_companyname",
            "value"=>$CurrentRecord->companyname,
            "size"=>80
                ,"maxlength"=>80
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Companyname field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_address1",
            "value"=>$CurrentRecord->address1,
            "size"=>80
                ,"maxlength"=>80
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Address1 field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_address2",
            "value"=>$CurrentRecord->address2,
            "size"=>80
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_city",
            "value"=>$CurrentRecord->city,
            "size"=>80
                ,"maxlength"=>80
                ,"minlength"=>1
                ,"length_e"=>"Please enter the City field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_state",
            "value"=>$CurrentRecord->state,
            "size"=>5
                ,"maxlength"=>5
                ,"minlength"=>1
                ,"length_e"=>"Please enter the State field."
                
            ));

            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_zip",
            "value"=>$CurrentRecord->zip,
            "size"=>20
                ,"maxlength"=>20
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Zip field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_country",
            "value"=>$CurrentRecord->country,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Country field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_phone",
            "value"=>$CurrentRecord->phone,
            "size"=>20
                ,"maxlength"=>20
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Phone field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_fax",
            "value"=>$CurrentRecord->fax,
            "size"=>20
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_tablewidth",
            "value"=>$CurrentRecord->tablewidth,
            "size"=>4
                ,"maxlength"=>4
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Tablewidth field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_tablealign",
            "value"=>$CurrentRecord->tablealign,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Tablealign field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_tableborder",
            "value"=>$CurrentRecord->tableborder,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Tableborder field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_tablecellpadding",
            "value"=>$CurrentRecord->tablecellpadding,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Tablecellpadding field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_tablecellspacing",
            "value"=>$CurrentRecord->tablecellspacing,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Tablecellspacing field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_textfg",
            "value"=>$CurrentRecord->textfg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Text fg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_pagebg",
            "value"=>$CurrentRecord->pagebg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Page bg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_link",
            "value"=>$CurrentRecord->link,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Link field."
                
            ));

            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_vlink",
            "value"=>$CurrentRecord->vlink,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Vlink field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_alink",
            "value"=>$CurrentRecord->alink,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Alink field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_headingfg",
            "value"=>$CurrentRecord->headingfg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Heading fg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_labelfg",
            "value"=>$CurrentRecord->labelfg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Label fg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_requiredfg",
            "value"=>$CurrentRecord->requiredfg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Required fg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_collabelfg",
            "value"=>$CurrentRecord->collabelfg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Collabel fg field."

            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_windowbg",
            "value"=>$CurrentRecord->windowbg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Window bg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_collabelbg",
            "value"=>$CurrentRecord->collabelbg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Collabel bg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_altcoldatabg",
            "value"=>$CurrentRecord->altcoldatabg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Altcoldatabg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_coldatabg",
            "value"=>$CurrentRecord->coldatabg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Coldata bg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_headingbg",
            "value"=>$CurrentRecord->headingbg,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Heading bg field."
                
            ));
            
            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_systemname",
            "value"=>$CurrentRecord->systemname,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the System name field."

            ));

            $form->add_element(array(
            "type"=>"textarea",
            "name"=>"field_stylesheet",
            "value"=>$CurrentRecord->stylesheet,
            "size"=>0
                ,"rows"=>6,
                "cols"=>60

            ));

            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_continue_url",
            "value"=>$CurrentRecord->continue_url,
            "size"=>40
                ,"maxlength"=>40
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Continue url field."

            ));

            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_custsvc_emailid",
            "value"=>$CurrentRecord->custsvc_emailid,
            "size"=>60
                ,"maxlength"=>60
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Custsvc emailid field."

            ));

            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_administrator_emailid",
            "value"=>$CurrentRecord->administrator_emailid,
            "size"=>60
                ,"maxlength"=>60
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Administrator emailid field."

            ));

            $form->add_element(array(
            "type"=>"text",
            "name"=>"field_sales_emailid",
            "value"=>$CurrentRecord->sales_emailid,
            "size"=>60
                ,"maxlength"=>60
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Sales emailid field."

            ));

            $form->add_element(array(
            "type"=>"textarea",
            "name"=>"field_htmlforgotpassword",
            "value"=>$CurrentRecord->htmlforgotpassword,
            "size"=>0
                ,"maxlength"=>0
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Htmlforgotpassword field."
                
                ,"rows"=>6,
                "cols"=>60
                
            ));
            
            $form->add_element(array(
            "type"=>"textarea",
            "name"=>"field_email_forgotpassword",
            "value"=>$CurrentRecord->email_forgotpassword,
            "size"=>0
                ,"maxlength"=>0
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Email forgotpassword field."
                
                ,"rows"=>6,
                "cols"=>60
                
            ));
            
            $form->add_element(array(
            "type"=>"textarea",
            "name"=>"field_htmlloginform",
            "value"=>$CurrentRecord->htmlloginform,
            "size"=>0
                ,"maxlength"=>0
                ,"minlength"=>1
                ,"length_e"=>"Please enter the Htmlloginform field."
                
                ,"rows"=>6,
                "cols"=>60
                
            ));

            $form->add_element(array(
            "type"=>"select",
            "name"=>"field_use_admin_settings",
            "value"=>$CurrentRecord->use_admin_settings,
            "options"=>YNSelectArray()
            ));


//if being called from myself, do validation..
if ($Submit) {



    if ($err=$form->validate() && ! isset($delete)) {
        echo $err;
    }
    else {
        // send to SQL
        if (isset($key)) {
            // UPDATE
            if ( ! isset($delete)) {
               $retval=DoUpdate($HTTP_POST_VARS,"sitesetup","site_no",$key);
            }
            else {
            // DELETE, CHECK FOR RELATED RECORDS IF REQUIRED

               $retval=DoDelete("sitesetup","site_no",$key);
            }
        }
        else {
            $retval=DoInsert($HTTP_POST_VARS,"sitesetup");
            if (isset($AddAnother)) {
                // was this a table with an SAPK?
                $key=mysql_insert_id();
                if (!$key) {
                    // no - there is a UAPK..
                    $key = $field_site_no;
                }
                // send the user back to this script, cause a do-over...
                Header("Location: sitesetup_edit.dtop?key=$key&do_over=1");
                exit();
            }
        }
        if ($retval)
            $form->SaveAndReturn("sitesetup","site_no",$key);
        return;
    }
}


//Begin HTML here
$DSCOLORPICKER=1;
$module_template_no = 999;  // use admin template
DSBeginPage("Site Setup",0,1,$module_template_no);

#TestAdminSettingsUsage();


#if (isset($delete)) {
#    //don't print javascript
#    $form->jvs_name="9999";
#}


$form->StartForm("Website Configuration");
if (isset( $delete ) || isset($view) )  {
    $form->freeze();
}
if (isset($delete))
    echo "Press SUBMIT to confirm deletion.";

$form->DrawField("field_site_no","Site no",1,0,0);
$form->DrawField("field_companyname","Companyname",1,0,0);
$form->DrawField("field_address1","Address1",1,0,0);
$form->DrawField("field_address2","Address2",0,0,0);
$form->DrawField("field_city","City",1,0,0);
$form->DrawField("field_state","State",1,0,0);
$form->DrawField("field_zip","Zip",1,0,0);
$form->DrawField("field_country","Country",1,0,0);
$form->DrawField("field_phone","Phone",1,0,0);
$form->DrawField("field_fax","Fax",0,0,0);
$form->DrawField("field_use_admin_settings","Default Settings for Admin Area?",1,0,0);
$form->DrawField("field_tablewidth","Tablewidth",1,0,0);
$form->DrawField("field_tablealign","Tablealign",1,0,0);
$form->DrawField("field_tableborder","Tableborder",1,0,0);
$form->DrawField("field_tablecellpadding","Tablecellpadding",1,0,0);
$form->DrawField("field_tablecellspacing","Tablecellspacing",1,0,0);
$form->DrawField("field_textfg","Text fg",1,0,0,DisplayColorPickerButton("field_textfg",""));
$form->DrawField("field_pagebg","Page bg",1,0,0,DisplayColorPickerButton("field_pagebg",""));
$form->DrawField("field_link","Link",1,0,0,DisplayColorPickerButton("field_link",""));
$form->DrawField("field_vlink","Vlink",1,0,0,DisplayColorPickerButton("field_vlink",""));
$form->DrawField("field_alink","Alink",1,0,0,DisplayColorPickerButton("field_alink",""));
$form->DrawField("field_headingfg","Heading fg",1,0,0,DisplayColorPickerButton("field_headingfg",""));
$form->DrawField("field_labelfg","Label fg",1,0,0,DisplayColorPickerButton("field_labelfg",""));
$form->DrawField("field_requiredfg","Required fg",1,0,0,DisplayColorPickerButton("field_requiredfg",""));
$form->DrawField("field_collabelfg","Collabel fg",1,0,0,DisplayColorPickerButton("field_collabelfg",""));
$form->DrawField("field_windowbg","Window bg",1,0,0,DisplayColorPickerButton("field_windowbg",""));
$form->DrawField("field_collabelbg","Collabel bg",1,0,0,DisplayColorPickerButton("field_collabelbg",""));
$form->DrawField("field_altcoldatabg","Altcoldatabg",1,0,0,DisplayColorPickerButton("field_altcoldatabg",""));
$form->DrawField("field_coldatabg","Coldata bg",1,0,0,DisplayColorPickerButton("field_coldatabg",""));
$form->DrawField("field_headingbg","Heading bg",1,0,0,DisplayColorPickerButton("field_headingbg",""));
$form->DrawField("field_systemname","System name",1,0,0);
$form->DrawField("field_stylesheet","Stylesheet",0,0,0);
$form->DrawField("field_continue_url","Continue url",1,0,0);
$form->DrawField("field_custsvc_emailid","Custsvc emailid",1,0,0);
$form->DrawField("field_administrator_emailid","Administrator emailid",1,0,0);
$form->DrawField("field_sales_emailid","Sales emailid",1,0,0);
$form->DrawField("field_htmlforgotpassword","Forgot Password Text:".DisplayHTMLEditButton("field_htmlforgotpassword"),1,0,0);
$form->DrawField("field_email_forgotpassword","Email forgotpassword",1,0,0);
$form->DrawField("field_htmlloginform","Login Text:".DisplayHTMLEditButton("field_htmlloginform"),1,0,0);
$form->EndForm(!isset($key));


// Save data back to database.

PreviousMenu( "admin.html" );
echo "<p>";

DSEndPage();
?>
