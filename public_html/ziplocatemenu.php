<?
########################################################################
# Copyright 2001 Desktop Solutions Software, Inc.
# 631-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
#########################################################################
require ("prepend.php3");
page_open(array("sess" => "DS_Session",
				"auth" => "DS_Auth",
				"perm" => "DS_Perm"));
if (!HasPermissions("admin")) {
    return;
}
#$DSDEBUG=1;
$module_template_no = 999;  // use admin template
$systemname = OneSQLValue( "select systemname from ziplocateoption where ziplocateoption_no = 1" );
DSBeginPage("$systemname Menu",0,1,$module_template_no);
StartDCMenu("$systemname Menu");
DrawDCMenuLine("$systemname Actions");
DrawDCMenuLine("Maintain List",'location_browse.dtop');
DrawDCMenuLine("Download List",'download.dtop?table=location');
DrawDCMenuLine("Upload List",'importcsv.dtop?tablename=location');
DrawDCMenuLine("Program Options",'ziplocateoption_edit.php');
DrawDCMenuLine("View Lookup Page",'ziplocate.html');
DrawDCMenuLine("Other Options");
DrawDCMenuLine("View $systemname Documentation",'http://www.desktopmodules.com/docs/zipfinder.pdf');
DrawDCMenuLine("Return to main menu",'admin.html');
EndDCMenu();
DSEndPage(1,0,$module_template_no); ##CET removed skip extra and added template

?>
