<?php
########################################################################
# Copyright 1999 Desktop Solutions Software, Inc.
# 631-493-3422
# info@dtopinc.com
# www.dtopinc.com
# usage rights granted for use on a per-project-basis
/*
 *   $Header: /home/cvs/lmp/project.inc,v 1.13 2001/02/10 15:16:37 tom Exp $
 */

#$DSDEBUG                    = 1; //if set to 1; SQL is echoed to the screen

#Database Name
$DSDATABASE                 = "nytherapyguide";
$DSHOST                     = "localhost";
$DSUSER                     = "nytherapyguide";
$DSPASSWORD                 = "carpe1";

#define colors used by library routines - OK to over-ride these
$DSTEXTFG                   = "#000000";//standard text color
$DSPAGEBG                   = "#CCCC99";//standard page bgd color
$DSLINK                     = "#000000";//default color of links
$DSVLINK                     = "#000000";//default color of links
$DSALINK                     = "RED";//default color of links
$DSHEADINGFG                = "BLACK";//text on bar on top & bottom
$DSLABELFG                  = "#111111";//labels 'First Name', etc.
$DSREQUIREDFG               = "RED";//labels when required
$DSCOLLABELFG               = "#000000";//text - browser columns

$DSHEADINGBG                = "#E0E0C1";//color of bar on top & bottom
$DSWINDOWBG                 = "#CCCC99";//bgd color of 'window' in entry form
$DSCOLLABELBG               = "#CCCC99";//bgd color for labels in browser
$DSCOLDATABG                = "#D6D6AD";//bgd color for data in browser

#Define some items for display purposes
$DSSYSTEMNAME               = "Lawrence Metal Products";
$DSOWNERNAME                = $DSSYTEMNAME;

#Define images
$DSIMAGEBG                  = ""; //standard background for pages
$DSADDBUTTON                = "/images/add.gif";
$DSEDITBUTTON               = "/images/edit.gif";
$DSDELBUTTON                = "/images/delete.gif";
$DSVIEWBUTTON               = "/images/view.gif";
$DSSTARTBUTTON              = "/images/start.gif";
$DSNEXTBUTTON               = "/images/next.gif";
$DSPREVBUTTON               = "/images/prev.gif";
$DSENDBUTTON                = "/images/end.gif";

$ERRORMAIL                  = "weberrors@lawrencemetal.com";
$MAILTO                     = "tom@desktopsolutions.com";
#$MAILTO                     = "tom@desktopsolutions.com,franki@desktopsolutions.com";

## Some functions that can be over-ridden in order to personalize the
## library routines
function DSBeginPage($Header="")
{
   DSHeading($Header);
 #   echo "
 #   <TABLE WIDTH=600 BORDER = 1 BGCOLOR=WHITE>
 #   <TR><TD ALIGN=CENTER><IMG SRC=images/logo50.gif></TD><TR>
 #  <TR><TD ALIGN=CENTER>
 #   ";
}
function DSEndPage($Menu=0)
{

    echo "
    </TD></TR>
    ";
    if ($Menu==0) {
#        echo "<TR><TD><center><A HREF=admin_menu.html>return to account menu </A></center></TD></TR>";
    }
    echo "
    </BODY>
    </HTML>";
}


######
# Function GoBack
# same as to save and return, but you can specify how far to
# go back
# Tom 12/14/99
######
Function GoBack($num=-2)
{
    echo "
    <HTML>
    <BODY>
    <script language=JavaScript>
        history.go($num);
    </script>
    </BODY>
    </HTML>
    ";
}

######
# Function ImgUpload
# Handleds the uploading of the image, including file-exists and
# patching for library
# Tom 3/2/00
# vars:
# $imgdir - full path to image /home/httpd/html/images
# $field_image - php upload image var
# $field_image_name - the string that is the name of the file ex: "test.jpg"
#   you get this from php, read about file uploads for more info
# $dbentry - the directory that gets appended to the file name and saved
#       to the database ex: /images/
# $fieldname - string containing name of field to be set
# $debug - prints out vars if there is an error
######
Function ImgUpload($imgdir,$field_image,$field_image_name,$dbentry,$fieldname="image",$debug=0)
{
        global $HTTP_POST_VARS;
        if ($field_image != "none") {
            $imgname = $imgdir . $field_image_name;
            $webname = $field_image_name;
            $counter=1;
            while ( file_exists($imgname) )
            {
                $imgname = $imgdir . $counter . $field_image_name;
                $webname = $counter . $field_image_name;
                $counter++;
            }

            if (!copy($field_image,$imgname)) {
                echo "<h2>Copy of $field_image_name failed</h2>\n";
            }
            else {


                $HTTP_POST_VARS[$fieldname] = $dbentry . $webname;

            }
        }

        if ($debug) {
            echo "field_image= " . $field_image. "<br>";
            echo "field_image_name= " . $field_image_name. "<br>";
            echo "imgdir = " . $imgdir . "<br>";
            echo "webname= ". $webname."<br>";
            echo "imgname = ". $imgname ."<br>";
            exit();
        }
}

#########
# Function PicList
# creates the piclist for the staff to use
# sent via email with EmailPicList()
#########
Function PicList($list_no)
{
    global $HTTP_POST_VARS;
    $message="";
    $listheader="";
    $itemdetail="";

    //list header info
    $q = DoQuery("SELECT list.*,CONCAT(auth_user.first,' ',auth_user.last) as authname FROM list,auth_user WHERE list_no = $list_no AND auth_user.uid = list.userid_ordered");
    $r = mysql_fetch_object($q);


$shipq  = "SELECT text FROM code WHERE category = 'shipmethd' AND code = '" .$r->shipmethod. "'";
$customerno = GetUsersMapixNo();
$shipmethod = OneSQLValue($shipq);
$listheader="Order Date: $r->dteorder
Ship To:
$r->shipname
$r->shipattention
$r->shipaddress1
$r->shipaddress2
$r->shipcity, $r->shipstate  $r->shipzip
$r->shipcountry
Phone: $r->shipphone
Fax: $r->shipfax

Customer Number: $customerno
Shipping Method: $shipmethod
Order Number: $r->list_no
Customer Purchase Order Number: $r->shipponumber
Cart Name: $r->title
Shipping Comment: $r->ship_comment
Order Comment: $r->order_comment
";
if (isset($HTTP_POST_VARS["x_auth_code"])) {
    $listheader.= "\n***This is a credit card order and it has been approved***\n";
    $listheader.= "Conversation with Credit Card Company follows:\n";
    $listheader .= "\nresponse_code: " . $HTTP_POST_VARS["x_response_code"];
    $listheader .= "\nresponse_subcode: " . $HTTP_POST_VARS["x_response_subcode"];
    $listheader .= "\nresponse_reason_code: " . $HTTP_POST_VARS["x_response_reason_code"];
    $listheader .= "\nresponse_reason_text: " . $HTTP_POST_VARS["x_response_reason_text"];
    $listheader .= "\nauth_code: " . $HTTP_POST_VARS["x_auth_code"];
    $listheader .= "\navs_code: " . $HTTP_POST_VARS["x_avs_code"];
    $listheader .= "\ntrans_id: " . $HTTP_POST_VARS["x_trans_id"];
    $listheader .= "\namount: " . $HTTP_POST_VARS["x_amount"];
    $listheader .= "\nmethod: " . $HTTP_POST_VARS["x_method"];
    $listheader .= "\ntype: " . $HTTP_POST_VARS["x_type"];
    $listheader .= "\nMD5_Hash: " . $HTTP_POST_VARS["x_MD5_Hash"];
}
    //itemdetail query
    $retval = DoQuery("SELECT * FROM listdetail WHERE list_no = $list_no");
    while ( $row = mysql_fetch_object($retval) ) {
        $itemdetail.= $row->quantity ."\t" . $row->model_no . "\n";
        if ( strlen($row->itemcomment) ) {
            $itemdetail .= "Item Comment: " .$row->itemcomment . "\n";
        }
    }


    $message = $listheader . $itemdetail;
    return $message;

}
##########
# Function EmailPicList($list_no,$subject="Order Received")
# sends the piclist to the shipping department
##########
Function EmailPicList($list_no,$subject="Order Received")
{

 $message = PicList($list_no);
 $emailaddress=OneSQLValue("SELECT email_shipid FROM siteowner");
 mail($emailaddress,$subject,
        $message,
        "From: weborders@lawrencemetal.com");


}

######
# Function GetSessionID2()
# COPIED FROM lmp.inc
###
Function GetSessionID2()
{
    global $sess;
    if ( isset($sess) ) {
        return $sess->id;
    }
}

################
# Function MakeCurrentCartId2
# if we don't know who the user is
# make a list for that user
# insert into list, then update active_sessions
# COPIED FROM configurator.inc
################
Function MakeCurrentCartId2($userid="")
{

    $sessid = GetSessionID2();
    # TJM - 10/20/00 user could be logged in and have record
    if (!$userid) { //not passed in
        $userid = OneSqlValue("SELECT uid FROM active_sessions WHERE sid = '$sessid'");
        if (!$userid) { //not in active sessions
            $userid = "NA";
        }
    }
#       srand((double)microtime()*1000000);
#       $randval = rand(1,100);

       $title = "Untitled Cart";
#       $title = "Untitled Cart" . $randval;

       $qlist = "INSERT INTO list
                   (userid_ordered, title, shipname, shipponumber,
                   shipattention,shipaddress1,shipaddress2,shipcity,
                   shipstate,shipzip,shipcountry,shipphone,shipfax,dteentry)
                   VALUES
                   ('$userid','$title','NA','NA','NA',
                   'NA','NA','NA','NA','NA','NA','NA','NA',NOW())";

    $retval = DoQuery($qlist);
    $newlist = mysql_insert_id();

    $qactive = "UPDATE
                active_sessions
                SET uid = '$userid', current_list = '$newlist'
                WHERE sid = '$sessid'";


    DoQuery($qactive);
    return $newlist;
}

##############
# function ErrorEmail
# sends error email to $MAILTO
##############
Function ErrorEmail($query="")
{
global $ERRORMAIL,$MAILTO,$SERVER_NAME;

$msg = "";
$msg .= $query . "\n";
$msg .= "Date/Time: " . Now() . "\n";
$msg .= mysql_error() . "\n";
$msg .= "-----------------The environment follows--------------\n";

        //dump all vars in the enviroment
    while (list($key, $val) = each($GLOBALS))
    {
        $msg .= "$key = $val\n";

    }


    mail (  $MAILTO,
            "Error Encountered: " . $SERVER_NAME,
            $msg,
            "From: " . $ERRORMAIL . "\n" );

}
######
#Function Now
#Return the time and date of Now
######
Function Now()
{
  return date("d-m-Y H:i:s");
}

######
# Function CheckoutMap($position)
# Return checkoutmap
######
Function CheckoutMap($position=1)
{

# $position=1;
 $cartstring="";
 $wordcolor="";
 $color=array();
 $returnstring="
 <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"98%\">
  <tr>
    ";

    for ($i=1;$i<=5;$i++) {
        $cartstring .="<td width=\"20%\" align=\"center\" valign=\"bottom\">";
        if ($i==$position) {
            $cartstring.="<img border=\"0\" src=\"images/checkoutcart.gif\" width=\"21\" height=\"22\">";
            $color[$i-1] = "maroon";
        }
        else {
            $cartstring.="&nbsp;";
            $color[$i-1] = "black";
        }
        $cartstring .="</td>";
    }
  $returnstring.=$cartstring;
  $returnstring.="
  </tr>
  <tr>
    <td width=\"100%\" colspan=\"5\" align=\"center\">
      <hr color=\"#000000\" width=\"90%\">
    </td>
  </tr>
  <tr>
    <td width=\"20%\" valign=\"top\">
      <p align=\"center\"><font face=\"Arial\" size=\"1\" color=\"$color[0]\"><b>shipping<br>
      info</b></font></td>
    <td width=\"20%\">
      <p align=\"center\"><font face=\"Arial\" size=\"1\" color=\"$color[1]\"><b>shipping <br>method</b></font></td>
    <td width=\"20%\">
      <p align=\"center\"><font size=\"1\" face=\"Arial\" color=\"$color[2]\"><b>payment</b></font></td>
    <td width=\"20%\">
      <p align=\"center\"><font size=\"1\" face=\"Arial\" color=\"$color[3]\"><b>confirm</b></font></td>
    <td width=\"20%\">
      <p align=\"center\"><font size=\"1\" face=\"Arial\" color=\"$color[4]\"><b>place <br>order</b></font></td>
  </tr>
</table>
  ";

    return $returnstring;
}

###########
# Function CheckOutWizard($noback=0,$nonext=0)
# draws the wizard buttons for checkout
# TJM 01/09/2001
###########
Function CheckOutWizard($noback=0,$nonext=0)
{
    global $DSHEADINGBG;
$returnstring .="<tr>";

    $returnstring.="<td width=\"50%\" bgcolor=\"$DSHEADINGBG\" align=\"left\">";
    if (!$noback) {
        $returnstring.="<input type=\"button\" name=\"previous\" Value=\"&lt;&lt;&nbsp;Back\" onClick='GoBack()'>";
    }
    else {
        $returnstring .="&nbsp;";
    }
    $returnstring.="</td>";

    $returnstring.="<td width=\"50%\" bgcolor=\"$DSHEADINGBG\" align=\"right\">";
    if (!$nonext) {
        $returnstring.="<input type=\"Submit\" name=\"Submit\" Value=\"Next&nbsp;&gt;&gt;\">";
    }
    else {
        $returnstring .="&nbsp;";
    }

    $returnstring.="</td>";

    $returnstring .="</tr>";

    return $returnstring;
}
?>
