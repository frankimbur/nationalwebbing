<?php


/*
 * $Header: /home/cvs/lmp/php/local.inc,v 1.3 2000/10/31 17:27:26 tom Exp $
 * Session Management for PHP3
 *
 * Copyright (c) 1998,1999 SH Online Dienst GmbH
 *                    Boris Erdmann, Kristian Koehntopp
 *
 * $Id: local.inc,v 1.3 2000/10/31 17:27:26 tom Exp $
 *
 */
$libpath = "post"; //salt for mysql's encode()

class DB_Example extends DB_Sql {
  var $Host     = "localhost";             ##
  var $Database = "nytherapyguide";                 ##
  var $User     = "nytherapyguide";                  ##
  var $Password = "carpe1";                      ##
}

class DS_CT_Sql extends CT_Sql {
  var $database_class = "DB_Example";          ## Which database to connect...
 var $database_table = "active_sessions"; ## and find our session data in this table.
}

class DS_Session extends Session {
  var $classname = "lawrencemetal";

  var $cookiename     = "";                ## defaults to classname
  var $magic          = "Hocuspocus";      ## ID seed
  var $mode           = "cookie";          ## We propagate session IDs with cookies
  var $fallback_mode  = "cookie";
  var $lifetime       = 240;                 ## 0 = do session cookies, else minutes
  var $that_class     = "DS_CT_Sql"; ## name of data storage container
  var $gc_probability = 0;
}

class DS_User extends User {
  var $classname = "DS_User";

  var $magic          = "Abracadabra";     ## ID seed
  var $that_class     = "DS_CT_Sql"; ## data storage container
}

class DS_Auth extends Auth {
  var $classname      = "DS_Auth";

  var $lifetime       =  500;

  var $database_class = "DB_Example";
  var $database_table = "auth_user";

  function auth_loginform() {
    global $sess;
    global $_PHPLIB;

    include($_PHPLIB["libdir"] . "loginform.ihtml");
  }

  function auth_validatelogin() {
    global $username, $password,$libpath;

    if(isset($username)) {
      $this->auth["uname"]=$username;        ## This provides access for "loginform.ihtml"
    }


	$uid = false;

    $this->db->query(sprintf("select uid, perms
                                     from %s
                                    where username = '%s'
                                    and password= encode('%s',\"$libpath\")
                                    and active='Y'",
                          $this->database_table,
                          addslashes($username),
                          addslashes($password)));

    while($this->db->next_record()) {
      $uid = $this->db->f("uid");
      $this->auth["perm"] = $this->db->f("perms");
    }
    #FTI 3/27/00 update lastlogon date and times logged in
    $this->db->query("
        update $this->database_table
        set
        lastlogon=now(),
        timesloggedon = timesloggedon+1
        where
        uid='$uid'");
    # FTI 6/13/00 update user ID in session table
    global $sess;
    DoQuery("UPDATE active_sessions SET uid='$uid' WHERE sid='$sess->id' ");
    return $uid;
  }
}


class DS_Perm extends Perm {
  var $classname = "DS_Perm";

  // this is the default permissions array that
  // was set up for us
  //var $permissions = array(
  //                          "user"       => 1,
  //                          "author"     => 2,
  //                          "editor"     => 4,
  //                          "supervisor" => 8,
  //                          "admin"      => 16
  //                        );

  // this is the permissions array that we are going to use for LMP
  // we're using inclusive permissions
  var $permissions = array("user" 		=> 1,
						   "dealer"		=> 3,
						   "dealeradmin"=> 7,
						   "lmpstaff"	=> 15,
						   "superuser"	=> 31);

  function perm_invalid($does_have, $must_have) {
    global $perm, $auth, $sess;
    global $_PHPLIB;

    include($_PHPLIB["libdir"] . "perminvalid.ihtml");
  }
}
?>
