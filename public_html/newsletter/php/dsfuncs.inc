<?php
/*
 * $Header: /home/cvs/lmp/php/dsfuncs.inc,v 1.10 2001/02/10 18:00:52 tom Exp $
 */
# $Id: dsfuncs.inc,v 1.10 2001/02/10 18:00:52 tom Exp $
// Desktop Solutions Library Functions
// (c) 1999  Desktop Solutions Software, Inc.
// Frank Imburgio 7/99
// 516 493 3422
// franki@dtopinc.com
require("table.inc");
require("oohforms.inc");
// all functions follow this naming convention for HTML form vars:
// field_first is a SQL field named first, etc.
// this allows the fields to be parsed by javascript &
// also allows selecting the SQL data from an assoc array by looking
// only at the array elements beginning with field_
//*******
Function NoCache()
//*******
{
   header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");             // Date in the past
   header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT"); // always modified
   header("Cache-Control: no-cache, must-revalidate");           // HTTP/1.1
   header("Pragma: no-cache");                                   // HTTP/1.0
}
//*****
Function DoUpdate($vars,$table,$key,$keyval,$isnum=0)
// Formats & Sends an update command when field.. vars are filled in
//*****
{
   $key = stripslashes($key);
   $valuelist = '';
   while (list($field, $val) = each($vars)) {
        if ( strstr($field,"field_")) {
            $field=substr($field,6);
            $valuelist .= "$field = '$val', ";
        }
    }
    $valuelist = ereg_replace(', $', '', $valuelist);
    if (!$isnum)
        $query = "UPDATE $table SET $valuelist WHERE $key = '$keyval'";
    else
        $query = "UPDATE $table SET $valuelist WHERE $key = $keyval";
    return DoQuery($query);
}
//*****
Function DoDelete($table,$key,$keyval)
// Formats and sends a delete command for a table with a key field & value
//*****
{
    $query = "DELETE FROM $table WHERE $key = '$keyval'";
    return DoQuery($query);
}
//****
Function DoInsert($vars,$table)
// Formats & Sends an insert command when field.. vars are filled in
//****
{
   $fieldlist = '';
   $valuelist = '';
    while (list($key, $val) = each($vars)) {
        if ( strstr($key,"field_")) {
            $key=substr($key,6);
            $fieldlist .= "$key, ";
            $valuelist .= "'$val', ";
        }
    }
    $fieldlist = ereg_replace(', $', '', $fieldlist);
    $valuelist = ereg_replace(', $', '', $valuelist);
    $query = "INSERT INTO $table ($fieldlist) VALUES ($valuelist)";
    return DoQuery($query);
}
//*****
Function DoQuery($query)
// Sends query to database engine, returns error if required
// returns result code
//*******
{
    global $DSDEBUG,$DSDATABASE,$DSHOST,$DSUSER,$DSPASSWORD;
    if ($DSDEBUG)
        echo "DEBUG:<BR>".$query."<BR>";

    mysql_connect($DSHOST,$DSUSER,$DSPASSWORD);
    $result=mysql_db_query($DSDATABASE,$query);

    if (! $result) {
        $msg=mysql_error();
        # check for duplicate error message
        if (strstr($msg,"Duplicate")) {
            echo "<H3>That is a duplicate entry.<BR>
            This data has not been saved.<BR>
            Please re-enter your information.</H3>
            ";
            if ($DSDEBUG) {
                echo $msg;
            }
        }
        else {
            # FTI 6/29/00 mail error if on live server
            global $HTTP_HOST,$DSSYSTEMNAME;
            if (strstr($HTTP_HOST,"lserv") || $DSDEBUG) {
                echo "
                <h3>Data has not been written!</h3>
                The database engine reported the following error:<br><i>
                ";
                echo mysql_error();
                echo
                "
                </i><br><br>The following information may be helpful for debugging:<br><i>
                $query</i>
                <a href=$PHP_SELF>use the back button</A>
                ";
            }
            else {
                ErrorEmail($query);
            }
        }
    }
    return $result;
}
//*****
function ReadCurrentRecord($table,$primary_key,$keyval,$isint=0)
// Reads current record into $CurrentRecord object
//*****
{
    if (isset($keyval)) {
        $myprimary_key = stripslashes($primary_key);
        if (! $isint) {
            $query =
            sprintf("SELECT * FROM $table WHERE $myprimary_key = '%s'",
            addslashes($keyval));
        }
        else {
            $query =
            sprintf("SELECT * FROM $table WHERE $myprimary_key = %s",
            $keyval);        }
        $result = DoQuery($query);
        $CurrentRecord = mysql_fetch_object($result);
        mysql_free_result($result);
    }
    else {
       $result = DoQuery("SHOW FIELDS FROM $table");
       $CurrentRecord = mysql_fetch_object($result);
       mysql_free_result($result);
    }
    return $CurrentRecord;
}
//*****
function DSHeading($Header)
// Outputs cache headers, titles, and meta tags with copyright
// Also sets colors to those from the project.inc file
//******
{
    global $DSSYSTEMNAME,$DSOWNERNAME,$DSPAGEBG,$DSTEXTFG,$DSIMAGEBG,$DSLINK,$DSVLINK,$DSALINK;
    NoCache();
    echo "
    <html>
    <head>
    ";
    if (strlen($Header))
        echo "<title>$DSSYSTEMNAME - $Header</title>";
    else
        echo "<title>$DSSYSTEMNAME</title>";
    echo "
    <meta name=comment content=\"This web site Copyright 1999 $DSOWNERNAME\">
    <meta name=comment content=\"uses library code Copyright 1999 Desktop Solutions- all rights reserved\">
    </head>
    <body bgcolor='$DSPAGEBG' text='$DSTEXTFG' background='$DSIMAGEBG' link='$DSLINK' vlink='$DSVLINK' alink='$DSALINK'>
    ";
}
function FormatMySqlDates($prefix)
// returns a string suitable for sending to MySql.
// assumes that three variables all start with the same prefix, and
// were form variables
// i.e. "firedate, firemonth, fireyear"
// pass the function 'fire'
{
    global $HTTP_POST_VARS;
    $yearv=$prefix."year";
    $monv=$prefix."month";
    $dayv=$prefix."day";
    return $HTTP_POST_VARS[$yearv]."-".$HTTP_POST_VARS[$monv]."-".$HTTP_POST_VARS[$dayv];
}
Function SaveAndReturn()
{
    echo "
    <HTML>
    <BODY>
    <script language=\"JavaScript\">
        history.go(-2);
    </script>
    </BODY>
    </HTML>
    ";
}
//******
class DSForm extends form
//******
{
    var $AddAnother = 0;
    function StartForm($Label) {
        global $PHP_SELF,$DSHEADINGBG,$DSHEADINGFG;
        $this->add_element(array(
                    "type"=>"submit",
                    "name"=>"Submit"
                      ));
        $this->start("entryform","POST",$PHP_SELF,"_self","entryform");
        echo
        "
        <table CELLSPACING='0' BORDER='0' WIDTH='95%'>
        <TR>
        <td bgcolor='$DSHEADINGBG' colspan=2 align='center' >
        <font color='$DSHEADINGFG'>$Label</FONT></td>
        </TR>
        ";
    }
    function EndForm($adding=0)
    {
        global $DSHEADINGBG,$DSHEADINGFG;
        echo "
        </tr>
        <tr> <td BGCOLOR='$DSHEADINGBG' colspan=2 align='right'>";
        if ($this->AddAnother && $adding) {
            echo " <font color=$DSHEADINGFG>Add Another:</font><input name=AddAnother Type=CheckBox > ";
        }
        $this->show_element("Submit","Submit");
        echo "
        <td>
        </table>";
        $this->finish();
    }
    function AddDate($Field,$CurrentRecord)
    {
        $this->add_element(array("type"=>"text",
                                "name"=>$Field."month",
                                "size"=>2,
                                "value"=>substr($CurrentRecord->$Field,5,2),
                                "valid_regex"=>"^[0-9]*$",
                                "valid_e"=>"Invalid month in $Field"));

        $this->add_element(array("type"=>"text",
                                "name"=>$Field."day",
                                "size"=>2,
                                "value"=>substr($CurrentRecord->$Field,8,2),
                                "valid_regex"=>"^[0-9]*$",
                                "valid_e"=>"Invalid day in $Field"));

        $this->add_element(array("type"=>"text",
                                "name"=>$Field."year",
                                "size"=>4,
                                "value"=>substr($CurrentRecord->$Field,0,4),
                                "valid_regex"=>"^[0-9]*$",
                                "valid_e"=>"Invalid year in $Field"));
        $this->add_element(array("type"=>"hidden",
                                "name"=>"field_".$Field,
                                "value"=>$CurrentRecord->$Field));
    }
    function DrawDate($Name,$Label,$Required=0,$NoCalendar=0)
    {
        $this->DrawField($Name."month",$Label,$Required,1);
        $this->show_element($Name."day");
        $this->show_element($Name."year");

        if ($NoCalendar)
            echo " <a href=php/calendar.dtop target=_blank>view calendar</a>";

        echo "</td>";
    }

    function DrawField($Name,$Label,$Required=0,$SkipTableClose=0,$SkipLabel=0)
    {
        global $DSLABELFG,$DSREQUIREDFG,$DSWINDOWBG;
        if ($Required)
            $color=$DSREQUIREDFG;
        else
            $color=$DSLABELFG;
        // label section
        if (!$SkipLabel) {
            echo "
            <tr>
            <td WIDTH='150' VALIGN=top BGCOLOR='$DSWINDOWBG'> <font color='$color'>$Label</font>
            </td>
            <td BGCOLOR='$DSWINDOWBG'>";
        }
        $this->show_element($Name);
        if ( ! $SkipTableClose)
            echo "</td>";
    }
    function FKLink($deleteorview,$table,$label,$returnhere="")
    {
        // ALL foo, not working yet...
        #return;
        if (!$deleteorview) {
            if (strlen($returnhere))
                $url = $table."_edit.dtop";
            else
                $url = $table."_edit.dtop";
            echo
            "
            <A HREF=$url>$label</A>
            ";
        }
        echo "</td>";
    }


######################
# Function DrawCC
# Wrapper that sets up generic credit card fields
# Set them with AddCC
# Tom 4/5/00 - based on code by John F
######################
Function DrawCC($required=1)
{

    $this->DrawField("field_creditcard_type","Type",$required,0,0);
    $this->DrawField("field_creditcard_no","Number",$required,0,0);
    $this->DrawField("field_creditcard_monthexp","Expiration Date <font size=-1>(MM/YY)</font>",$required,1,0);
    $this->DrawField("field_creditcard_yearexp","Year", $required,0,1);

}


######################
# Function AddCC
# Wrapper that sets up generic credit card fields
# Display them with DrawCC
# Tom 4/5/00 - based on code by John F
# $CurrentRecord - required. Handle to the current record
#                   This will be used to set the default values of the elements
# $typequery - optional.  If set, this will be used instead of default query
#                   which looks for a table called creditcard_types
#$ccnum,$ccmon,$ccyear,$cctype - names for fields
######################

Function AddCC($CurrentRecord,$typequery="",$ccnum="creditcard_no",$ccmon="creditcard_monthexp",
                    $ccyear="creditcard_yearexp",
                    $cctype="creditcard_type")
{

			$this->add_element(array(
            "type"=>"text",
            "name"=>"field_".$ccnum,
            "value"=>$CurrentRecord->$ccnum,
            "size"=>20
                ,"maxlength"=>20,
                "minlength"=>1,
                "length_e"=>"Please enter the Credit Card Number field."

            ));



			$this->add_element(array(
            "type"=>"select",
            "name"=>"field_".$ccmon,
            "value"=>$CurrentRecord->$ccmon,
			"options"=>MonthSelectArray(),
            "size"=>1
                ,"maxlength"=>2,
                "minlength"=>1,
                "valid_e"=>"Please enter the Credit Card Expiration Month field."

            ));

			$this->add_element(array(
            "type"=>"select",
            "name"=>"field_".$ccyear,
			"options"=>YearSelectArray(),
            "value"=>$CurrentRecord->$ccyear,
            "size"=>1
                ,"maxlength"=>4,
                "minlength"=>4,
                "valid_e"=>"Please enter the Credit Card Expiration Year field."

            ));

            if (!strlen($typequery))
                $typequery = "SELECT creditcard_type FROM creditcard_types";


			$sqlarray = FillSqlArray($typequery,1);

			$this->add_element(array(
            "type"=>"select",
            "name"=>"field_".$cctype,
			"options"=>$sqlarray,
            "value"=>$CurrentRecord->creditcard_type,
            "size"=>1
                ,"maxlength"=>20,
                "minlength"=>1,
                "length_e"=>"Please enter the Credit Card Type field."

            ));


}

} //end DSForm

//****************************
class DSBrowse extends Table {
//****************************
### required variables ##################################

#   (some are commented because they are declared in Table)
    var $Key;
#        must be a column name, spelled just like in select statement
#   $classname
#        must be case sensitive name of the table - used to build _edit.dtop calls
#   $fields
#        column headings - may be less than specified in select (see hidefrom)
    var $Columns = "";
#        ie "first,last,contact_no" - will be used in select statement
    var $db;
#        must be set to a database object

### optional variables ##################################

    var $Font = "Face='Arial' Size='2'";
#        font used to create whole browse box
    var $TopHeading="Data Listing";
#        Appears on first line
    var $HideFrom = 0;
#        Any columns beyond this # will not be selected, but not
#        displayed - good for hiding system assigned keys
    var $ExtraUrl = "";
#        if supplied, will be passed to edit routine. Can include vars
    var $NoEditing = 0;
    var $NoAdding = 0;
#        flag to turn off add/edit/delete buttons - delete & edit go together
    var $ViewButton = 0;
#        shows a different button in place of edit buttons, which must be turned off
#        please see $ViewURL to set the URL of the button
    var $DisplayRows = 10;
#       if <> 0, will cause next/prev arrow buttons
#       you can also change this to alter the # of rows on each screen
    var $WhereClause = "";
#        ie "salary > 0" - will be tacked on to every where clause built
    var $SearchChoices;
#        the array that will display in drop down for searching
#        i.e. "First Name"=>"first"
    var $TableName;
#       if specified, can be a list of tables. Defaults to $class.
    var $ChangeOrder;
#       a flag which will make the column headings clickable to adjust order
    var $LinkColumns=Array();
#       an array which can hold keyed pairs of column names and  prefixes to
#       display data as links. Example:
#       $t->LinkColumns = array("domain"=>"http://","email"=>"mailto:");
    var $AddUrl;
#       (optional) - if specified, the script url that will be linked to the
#       add button. If not specified, this url will be built from class name
#       plus '_add.dtop'.
    var $EditUrl;
#       (optional) - if specified, the script url that will be linked to the
#       edit button. If not specified, this url will be built from class name
#       plus '_edit.dtop'.
    var $DeleteUrl;
#       (optional) - if specified, the script url that will be linked to the
#       delete button. If not specified, this url will be built from class name
#       plus '_edit.dtop'.
    var $ViewURL;
#       (optional) - (the edit buttons must be turned off) if specified, the script url that will be linked to the
#       ViewButton. If not specified, this url will be built from class name
#       plus '_view.dtop'.
    var $OrderBy;
#       an optional orderby clause - do not include 'order by' defaults to '1'
#       if the user changes the searchby field, this will be changed
### used internally    ##################################

    var $Keyval="";
    var $HitHeaders=0;
    var $currentrow = 0;
    var $totalrows;
    var $rowsfetched;

## This class relies on a few params to be constantly passed to itself as arrow buttons are pushed and
## searches are performed, calling itself recursively. These params all begin with r_:
## r_limitrow,r_search,r_searchfield,r_orderby

    function display()
    {
        global $DSDEBUG,$r_limitrow,$r_search,$r_orderby,$r_searchfield;

        if (!strlen($this->TableName))
            $this->TableName=$this->classname;

        // Build initial SQL query without row limits
        $sql =
        "SELECT $this->Columns FROM $this->TableName ";

        if (strlen($r_search) || strlen($this->WhereClause) )
                $sql .= " WHERE ";

        if (strlen($r_search)) {
             $sql .= "$r_searchfield like \"$r_search%\"";
             if (strlen($this->WhereClause))
                $sql .= " AND ";
        }

        if (strlen($this->WhereClause) )
                $sql .= $this->WhereClause;

        #if (strlen($r_searchfield) && strlen($r_searchterm) ) {
        #    $r_orderby = $r_searchfield;
        #}
        if (isset($this->OrderBy) && ! isset($r_orderby)) {
            $r_orderby = $this->OrderBy;
        }
        if (!strlen($r_orderby)) {
            $r_orderby = 1;
        }
        $sql .= " ORDER BY $r_orderby ";

        // process the query and find out how many rows there are
        $this->db->query($sql);
        $this->totalrows=$this->db->num_rows();
        // DisplayRows set to 0 - display all the rows and go home..
        if (! $this->DisplayRows) {
            if ($DSDEBUG)
                echo $sql;
            $this->show_result($this->db,"data");
            return;
        }
        $this->currentrow = $r_limitrow; //store this row for use in table_open()
        if (!$r_limitrow || $r_limitrow < 0) { // any invalid non-numeric
            $r_limitrow = $this->currentrow = 0;
        }

        $sql .= " LIMIT $r_limitrow,$this->DisplayRows";
        $this->db->query($sql);

        $this->rowsfetched=$this->db->num_rows();
        if ($DSDEBUG)
            echo $sql;
        $this->show_result($this->db,"data");
    }
    function table_close()
    {
    if (count($this->SearchChoices))
        echo "</form>";
    echo "</table>
        ";
    }
    function table_open($class="")
    {
        global $DSHEADINGBG,$DSHEADINGFG,$DSADDBUTTON,
               $DSSTARTBUTTON,$DSNEXTBUTTON,$DSPREVBUTTON,$DSENDBUTTON,
               $PHP_SELF,$r_search,$r_searchfield,$r_orderby;
        $this->heading = "on"; // our class always prints headings
        $numfields=count($this->fields)+1;
        if (count($this->SearchChoices)) {
            $searchhtml = "
            <SELECT name=r_searchfield> ";
            while ($slice=each($this->SearchChoices)) {
                $searchhtml .= "<option ";
                if ($slice[1]==$r_searchfield)
                    $searchhtml .= " selected ";
                $searchhtml .= "value=$slice[1]>$slice[0]" ;
            }
            $searchhtml .=
            "</SELECT>
            =
            <input type=text
            name=r_search value=\"$r_search\" size=10>";
        }

        if ($this->DisplayRows) {
            $url = "&r_search=$r_search&r_searchfield=$r_searchfield&r_orderby=$r_orderby";
            if (strlen($this->ExtraUrl)) {
                $url .= '&'.$this->ExtraUrl;
            }
            $prevrow = $this->currentrow - $this->DisplayRows;
            $nextrow = $this->currentrow + $this->DisplayRows;
            $lastrow = $this->totalrows - ($this->totalrows % $this->DisplayRows);
            if ($lastrow == $this->totalrows)
                $lastrow -= $this->DisplayRows;

            if ($prevrow < 0)
                $prevrow = 0;
            if ($nextrow > $this->totalrows)
                $nextrow = $lastrow;

            $dispfirstrow = $this->currentrow+1;
            $displastrow  = $dispfirstrow + $this->rowsfetched - 1;

            $nextprevbuttons =
            "
            ($dispfirstrow-$displastrow of $this->totalrows)

            <A HREF='$PHP_SELF?r_limitrow=0$url'>
            <img src='$DSSTARTBUTTON' ALT='&lt&lt' width=23 height=15 border=0></a>

            <A HREF='$PHP_SELF?r_limitrow=$prevrow$url'>
            <img src='$DSPREVBUTTON' ALT='&lt' width=23 height=15 border=0></a>

            <A HREF='$PHP_SELF?r_limitrow=$nextrow$url'>
            <img src='$DSNEXTBUTTON' ALT='&gt' width=23 height=15 border=0></a>

            <A HREF='$PHP_SELF?r_limitrow=$lastrow$url'>
            <img src='$DSENDBUTTON' ALT='&gt' width=23 height=15 border=0></a>
            ";
        }
        else {
            $nextprevbuttons = "";
        }
        if (!$this->NoAdding) {
            if (!strlen($this->AddUrl)) {
                $url = strtolower($this->classname);
                $this->AddUrl=$url."_edit.dtop";
            }
            if (strlen($this->ExtraUrl)) {
                $this->AddUrl .= '?'.$this->ExtraUrl;
            }
            $addbutton = "<A HREF='$this->AddUrl'>
            <img src='$DSADDBUTTON' ALT='Add' width='23' height='15' border=0></A>";
        }
        else {
            $addbutton = "";
        }
        if (!isset($this->TableWidth))
            $this->TableWidth="95%";
        if (count($this->SearchChoices))
            echo "<form action=$PHP_SELF>";
        echo "
        <table border='0' cellpadding='0' cellspacing='0' width='$this->TableWidth'>
          <tr>
            <td NOWRAP bgcolor=$DSHEADINGBG align='left' valign='top'>
            <font color=$DSHEADINGFG face='Arial' size='2'>
            <b>$this->TopHeading</b></font>
            </td>
            <td NOWRAP bgcolor=$DSHEADINGBG align='right'>
            <font color=$DSHEADINGFG face='Arial' size='2'>
            $searchhtml $nextprevbuttons $addbutton
            </font></td>
          </tr>
          </table>
         <table border='1' cellpadding='0' cellspacing='0' width='$this->TableWidth'>
        ";
    }
    function table_row_close($row)
    {

        global $DSCOLLABELBG,$DSDELBUTTON,$DSEDITBUTTON,$DSCOLDATABG,$DSVIEWBUTTON;
        $Bgd = $DSCOLLABELBG;
        if (! $row && ! $this->HitHeaders) {
           $this->HitHeaders = 1;
            // end of heading row - no buttons
            echo "</TR>";
            return;
        }

        if (!strlen($this->ViewURL)) {
            $viewurl=strtolower($this->classname)."_view.dtop";
        }
        else {
            $viewurl .= $viewurl . $this->ViewURL;
        }
        $viewurl .= "?key=".urlencode($this->Keyval);

            if (strlen($this->ViewURL) && strlen($this->ExtraURL))
                $viewurl.= '&'. $this->ExtraURL;

        if ($this->NoEditing && ! isset($this->CheckName)) {
            if ($this->ViewButton)
                echo "<td NOWRAP colspan = 2 BGCOLOR='$DSCOLDATABG' align=right><a href=$viewurl><img src='$DSVIEWBUTTON' border=0></A></td></tr>";
            else
                echo "<td NOWRAP colspan = 2 BGCOLOR='$DSCOLDATABG'>&nbsp;</td></tr>";
            return;
        }
        if (isset($this->CheckName)) {
            $checked = "";
            reset($this->CheckKeys);
            while (list($key, $val) = each($this->CheckKeys)) {
                if ($this->Keyval == $val)
                    $checked = "CHECKED";

            }
            $buttons="<input type=checkbox name=$this->CheckName[$row] $checked value=$this->Keyval>";
        }
        if (!strlen($this->EditUrl))
            $editurl=strtolower($this->classname)."_edit.dtop";
        else
            $editurl=$this->EditUrl;

        if (!strlen($this->DeleteUrl))
            $deleteurl=strtolower($this->classname)."_edit.dtop";
        else
            $deleteurl=$this->DeleteUrl;
        if (!$this->NoEditing) {
            $deleteurl .= "?key=" . urlencode($this->Keyval) . "&delete=1";
            $editurl .= "?key=" . urlencode($this->Keyval);
            if (strlen($this->ExtraUrl)) {
                $editurl .= '&'.$this->ExtraUrl;
                $deleteurl .= '&'.$this->ExtraUrl;
            }
            $buttons .= "
            <A HREF='$deleteurl'>
            <img src='$DSDELBUTTON' Alt='Del' width='23' height='15' border=0></A>
            <A HREF='$editurl'>
            <img src='$DSEDITBUTTON' Alt='Edit' width='23' height='15' border=0></A>
            ";
            $Bgd=$DSCOLDATABG;
        }
        echo "
        <TD NOWRAP ALIGN='right' COLSPAN=2 BGCOLOR='$Bgd'>$buttons</TD></TR>
        ";
    }
    function table_row_open($row,$data,$class="")
    {
        global $DSCOLDATABG;
        echo "
        <tr BGCOLOR='$DSCOLDATABG' align='left'>
        ";
    }
    function table_cell($row,$cell,$key,$val,$class)
    {
        if ($key==$this->Key) {
            $this->Keyval = $val;
        }
        if (!strlen($val))
            $val='&nbsp';
        if ($this->HideFrom && $cell>= $this->HideFrom)
            return;
        if (strlen($this->LinkColumns[$key])) {
            $prefix=$this->LinkColumns[$key];
            echo "<TD NOWRAP><FONT $this->Font>
            <A HREF=$prefix$val target=_blank>$val</A></FONT></TD>";
        }
        else {
            echo "<TD NOWRAP><FONT $this->Font>$val</FONT></TD>";
        }
    }
    function table_heading_cell($col,$val,$class="")
    {
        global $DSCOLLABELBG,$DSCOLLABELFG,$DSADDBUTTON,
               $PHP_SELF,$r_search,$r_searchfield,$r_orderby;

        $sortorder = $col+1;
        $url = "&r_search=$r_search&r_searchfield=$r_searchfield&r_orderby=$sortorder";
        $val = ucwords($val);
        echo
        "<td NOWRAP BGCOLOR='$DSCOLLABELBG'>
            <font $this->Font COLOR='$DSCOLLABELFG'><b>";
        if ($this->ChangeOrder)
            echo "<A HREF=$PHP_SELF?$url>$val</A>";
        else
            echo "$val";
        echo "
            </b></font></td>
         ";
        if ( $col >= $this->HideFrom-1) {
            echo
            "<td NOWRAP BGCOLOR='$DSCOLLABELBG' COLSPAN=2>
                <font $this->Font COLOR='$DSCOLLABELFG'><b>
                &nbsp
                </b></font></td>
             ";
        }
    }
}
//*******
function RICheck($table,$field,$keyval)
//*******
{
    $RImsg="";
    $query = sprintf("SELECT count(*) FROM %s where %s='%s'",$table,$field,$keyval);
    $result = DoQuery($query);
    if ($result) {
        $row = mysql_fetch_array($result);
        if ($row[0] > 0) {
            $RIMsg =
            sprintf("That record is used %d time(s) in the <i>%s</i> table.<br>",
            $row[0],
            $table);
            return $RIMsg;
        }
        else {
            return "";
        }
    }
    return "ERROR FORM RICheck()";
}
//************
function ReportRIError($errmsg,$table,$keyval)
//************
{
    DSHeading("Delete Error - Can not delete!");
    echo "
    <br>
    $errmsg
    <h3>In order to delete this record from the <i>$table</i> table you must first find and delete all the above records.
    ";
    return;
}
//************
function RICascade($table,$field,$oldval,$keyval)
//************
{
    $query = sprintf("UPDATE %s SET %s='%s' WHERE %s='%s'",
    $table,$field,$keyval,$field,$oldval);
    return DoQuery($query);
}
//***********
function FillSQLArray($query,$addblankrow=0)
//***********
{
    $result = DoQuery($query);
    $loops=0;
    if ($addblankrow) {
        $retarray[$loops]["label"]="-- select one --";
        $retarray[$loops++]["value"]="";

    }
    if ($result) {
        while($row = mysql_fetch_array($result)) {
            $retarray[$loops++]=$row[0];
        }
        mysql_free_result($result);
        return $retarray;
    }
    return "";
}
//***********
function FillSQLArrayTwo($query,$addblankrow=0)
// Will fill a multi dimensional array with exactly two columns
// formatted for use with OOForms select type
//***********
{
    $result = DoQuery($query);
    $loops=0;
    if ($addblankrow) {
        $retarray[$loops]["label"]="-- select one --";
        $retarray[$loops++]["value"]="";

    }
    if ($result) {
        while($row = mysql_fetch_array($result)) {
              $retarray[$loops]["label"]=$row[0];
              $retarray[$loops++]["value"]=$row[1];
        }
        mysql_free_result($result);
        return $retarray;
    }
    return Array("");
}
//***********
function YNSelectArray()
//***********
// Returns a string appropriate for a Yes/No select array
// What the library uses for yes/no checkbox situations
{
    $yesno = array(array("label"=>"Yes","value"=>"Y"),
             array("label"=>"No","value"=>"N"));
    return $yesno;
}

//***********
function MonthSelectArray()
//***********
// Returns a string appropriate for a Month  select array
{
    $montharray = array(array("label"=>"--select one--","value"=>""),
    array("label"=>"January","value"=>"01"),
             array("label"=>"February","value"=>"02"),
		array("label"=>"March","value"=>"03"),
		array("label"=>"April","value"=>"04"),
		array("label"=>"May","value"=>"05"),
		array("label"=>"June","value"=>"06"),
		array("label"=>"July","value"=>"07"),
		array("label"=>"August","value"=>"08"),
		array("label"=>"September","value"=>"09"),
		array("label"=>"October","value"=>"10"),
		array("label"=>"November","value"=>"11"),
		array("label"=>"December","value"=>"12")
    );

return $montharray;
}

###################
# Function YearSelectArray
# Formats an array of 4 digit years for a
# select box - automatically grabs the current year
# $yearstoshow = how many years AFTER the currrent
#   i.e. if the current year is 2000 and $yearstoshow = 6,
#   then it will display 2000-2006
# used for things like credit card exp year
###################
Function YearSelectArray($yearstoshow=0,$default=1)
{
    if ($yearstoshow==0)
        $yearstoshow = 3;   //how many years AFTER the currrent

    $currentyear = date("Y");
    $yearsarray = array();
    $i=0;
    if ($default) {
        $yearsarray[$i] = array("label"=>"-- select one --","value"=>"");
        $i++;
        $yearstoshow++; //to compensate for the first element being filled in
    }
    for ($i; $i<=$yearstoshow;$i++)
    {
        $yearsarray[$i] = array("label"=>$currentyear,"value"=>$currentyear);
        $currentyear++;
    }

    return $yearsarray;
}

//***********
function FormatHtmlDates($date,$HTTP_POST_VARS)
//***********
{
    $yearv=$date."year";
    $monv=$date."month";
    $dayv=$date."day";
    return $HTTP_POST_VARS[$yearv]."-".$HTTP_POST_VARS[$monv]."-".$HTTP_POST_VARS[$dayv];
}

###################
# Function ImportFileToTable
# 
# this function reads a fixed width file named $file_name into the
# database table specified in $database_table
# 
# the $nodecimal parameter should be TRUE if something like 100.00 should be read as 10000
# by John F
###################
function ImportFileToTable($file_name, $database_table, $nodecimal)
{
	global $DSUSER,$DSPASSWORD,$DSHOST,$DSDATABASE;

	// returns: the number of records successfully imported to the database
	$numrecordsimported = 0;

	// first, make sure we can open the file and database connection

	// attempt to open file
	$file = fopen($file_name, "r");
	if (!$file)
	{
		echo "Error opening file: <strong>".$file_name."</strong><br>";
		return $numrecordsimported; // $numrecordsimported is still 0 at this point
	}

	// attempt to open database connection
	if (!mysql_connect($DSHOST,$DSUSER,$DSPASSWORD))
	{
		echo "mysql_connect failed : server: <strong>".$DSHOST."</strong>, User: <strong>".$DSUSER."</strong>, Password: <strong>".$DSPASSWORD."</strong><br>";
		echo mysql_error()."<br>";
		return $numrecordsimported; // $numrecordsimported is still 0 at this point
	}
	
	if (!mysql_select_db($DSDATABASE))
	{
		echo "mysql_select_db failed : database <strong>".$DSDATABASE."</strong> on server <strong>".$DSHOST."</strong>, User: <strong>".$DSUSER."</strong>, Password: <strong>".$DSPASSWORD."</strong><br>";
		echo mysql_error()."<br>";
		return $numrecordsimported; // $numrecordsimported is still 0 at this point
	}

	// now lets make sure the table we want exists by trying to query it
	$query = sprintf("select * from %s", $database_table);
	$result = mysql_query($query);

	if (!$result)
	{
		echo "Error querying table <strong>".$database_table."</strong> in database <strong>".$DSDATABASE."</strong> on server <strong>".$DSHOST."</strong><br>";
		echo mysql_error()."<br>";
		return $numrecordsimported; // $numrecordsimported is still 0 at this point
	}

	// at this point we know the filename and all the database connection
	// information are correct.  So, let's start importing the file

	// get number of fields
	$numfields = mysql_num_fields($result);

	// figure out length of a line by adding all field lengths
	$linelength = 0;
	$index = 0;
	while ($index < $numfields)
	{
		$linelength += mysql_field_len($result, $index);
		$index++;
	}

	// loop through file line by line until EOF is encountered
	while(!feof($file))
	{
		// read a line from the file
		$line = fgets($file, ($linelength + 3));

		// if line is too short, stop reading from this file
		if (strlen($line) < $linelength)
			break;

		// the beginnings of the insert statement
		$insertstring = sprintf("INSERT INTO %s VALUES (", $database_table);

		// control index for field parsing loop
		$index = 0;

		$stringindex = 0;

		// this inner loop parses through fields and constructs the insert statement
		while ($index < $numfields)
		{
			// get type and length of field
			$field_type		= mysql_field_type($result, $index);
			$field_length	= mysql_field_len($result, $index);

			// get field
			$field = substr($line, $stringindex, $field_length);
			$stringindex += $field_length;

			if ($field_type == "string")
			{
				$insertstring = $insertstring."'".$field."'";
			}
			else if ($field_type == "int")
			{
				$insertstring = $insertstring.$field;
			}
			else if ($field_type == "real")
			{
				if ($nodecimal)
				{
					// last two digits are hundreths (cents)
					$whole	 = substr($field, 0, ($field_length - 2));
					$decimal = substr($field, ($field_length - 2), 2);
					
					$newfield = sprintf("%s.%s", $whole, $decimal);
					$insertstring = $insertstring.$newfield;
				}
				else
				{
					$insertstring = $insertstring.$field;
				}
			}

			// insert a comma if this is not the last field
			if ($index != ($numfields - 1))
				$insertstring = $insertstring.",";
			
			// increment control index
			$index++;
		}

		// close out insert statement
		$insertstring = $insertstring.")";

		// show the insert statement
		if ($DSDEBUG)
			echo $insertstring."<br>";

		// attempt database update
		// if successful, 
		if (mysql_query($insertstring))
		{
			$numrecordsimported++;
		}
		else
		{
			// this shouldn't have happened...
			$message = sprintf("Error inserting record %d into table <strong>%s</strong> in database <strong>%s</strong> on server <strong>%s</strong><br>%s<br>SQL: <strong>%s</strong><br>%s<p>",$numrecordsimported, $database_table, $DSDATABASE, $DSHOST, mysql_error(), $insertstring, $line);
			echo $message;
			break;
		}
	}

	// we're done, close database connection and file
	mysql_close();
	fclose($file);

	// return number of records imported
	return $numrecordsimported;
}
#############
function OneSQLValue($sql)
#############
# returns a single column from a SQL query
# blank string if not found
{
    $result=DoQuery($sql);
    if ($result) {
        $Record=mysql_fetch_array($result);
        mysql_free_result($result);
        return $Record[0];
    }
    else {
        return $result;
    }
}
###########
# Function MySqlDate
# Formats a Mysql Date to human readable
###########
Function MySqlDate($datetoformat,$notime=0)
{
    $retval = DoQuery("SELECT DATE_FORMAT('$datetoformat','%m/%d/%Y %r') as datetoformat");
    $row=mysql_fetch_object($retval);
    $d = $row->datetoformat;
    if ($notime) {
        $d = substr($d,0,strpos($d," "));
    }
    return $d;

}
###############
Function DrawDropDownHTML($sql,$name,$default,$pleaseselect=0)
###############
# Draws HTML for a drop down from a code table with two fields
# for when form class is not being used
# Supplies default choice, too
# Example SQL - SELECT Data,Code from codetable order by Data
{
    $result=DoQuery($sql);
    $cols = mysql_num_fields($result);
    echo "<select NAME=$name>\r";
    if ($pleaseselect==1) {
        echo "<OPTION value=\"\">--Please Select--";
    }
    while ($Record=mysql_fetch_array($result))  {
        $data0=trim($Record[0]);
        $data1=trim($Record[1]);
        echo "<OPTION ";
        if ($data1 == $default) {
            echo " SELECTED ";
        }
        echo "VALUE=\"$data1\">$data0";
    }
    mysql_free_result($result);
    echo "</select>\r";
}

###############
Function ReturnDropDownHTML($sql,$name,$default,$pleaseselect=0,$selecttags=1)
###############
# SAME FUNCTION AS ABOVE, but returns code in a string rather than echo
# Draws HTML for a drop down from a code table with two fields
# for when form class is not being used
# Supplies default choice, too
# Example SQL - SELECT Data,Code from codetable order by Data
{
    $returnstring = "";
    $result=DoQuery($sql);
    $cols = mysql_num_fields($result);
    if ($selecttags) {
        $returnstring.= "<select NAME=$name>\r";
    }
    if ($pleaseselect==1) {
        $returnstring.= "<OPTION value=\"\">--Please Select--";
    }
    while ($Record=mysql_fetch_array($result))  {
        $data0=trim($Record[0]);
        $data1=trim($Record[1]);
        $returnstring.= "<OPTION ";
        if ($data1 == $default) {
            $returnstring.= " SELECTED ";
        }
        $returnstring.= "VALUE=\"$data1\">$data0";
    }
    mysql_free_result($result);

    if ($selecttags) {
        $returnstring.= "</select>\n";
    }
    return $returnstring;
}


###############
Function ReturnDropDownHTMLWD($sql,$name="",$default,$selectags=1,$pleaseselect=0,$sep="/")
###############
# SAME FUNCTION AS ABOVE, but calculates discount
# assumes three fields, price is last, gets concat to text to be displayed
# $sep is what will seperate the text in the drop down box
# Example SQL - SELECT Data,Code from codetable order by Data
{
    $returnstring = "";
    $result=DoQuery($sql);
    $cols = mysql_num_fields($result);
    $pricefield = $cols-1;
    while ($Record=mysql_fetch_array($result))  {
        $data0=trim($Record[0]);
        $data0.=$sep . "$".number_format(trim($Record[2]),2);
        $discount = CalcUserDiscount($Record[2]);
        if ($discount) {
            $data0.=$sep . "$".number_format($discount,2);
        }
        $data1=trim($Record[1]);    //the actual value
        $returnstring.= "<OPTION ";
        if ($data1 == $default) {
            $returnstring.= " SELECTED ";
        }
        $returnstring.= "VALUE=\"$data1\">$data0";
    }
    mysql_free_result($result);

    if ($pleaseselect==1) {
        $startstring= "<OPTION value=\"\">--Please Select--" . $sep . "List Price";
        if ($discount) {
            $startstring.= $sep . "Your Price";
        }
    }

    $returnstring = $startstring . $returnstring;
    if ($selectags) {
        $returnstring= "<select NAME=$name>\n" . $returnstring . "</select>\n";
    }
    return $returnstring;
}



//************
// END OF FILE
//************
?>
