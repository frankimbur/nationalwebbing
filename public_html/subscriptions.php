<?php
########################################################################
# Copyright 2001 Desktop Solutions Software, Inc.
# 631-493-3422
# info@desktopsolutions.com
# www.desktopsolutions.com
# usage rights granted for use on a per-project-basis
#########################################################################
# nl_subscription.php - allows a user to add or remove themselves from the list
require ("prepend.php3");
require "newsletter.inc";

#$DSDEBUG=1;
DSBeginPage("Subscription Management",0,1,$module_template_no);

if (! $Submit) {
    $subscribe_html=OneSQLValue("SELECT html_subscribe from newsletteroption where newsletteroption_no=1");
    $UsersRecord=GetUsersRecord();
    $form = new DSForm;
    $form->add_element(array(
        "type"=>"text",
        "name"=>"email",
        "value"=>$UsersRecord->username,
        "size"=>40
        ));
    $form->add_element(array(
        "type"=>"radio",
        "name"=>"subscribe",
        "value"=>"YES"
        ));
    $form->add_element(array(
        "type"=>"radio",
        "name"=>"subscribe",
        "value"=>"NO"
        ));
        echo $subscribe_html;
        $form->StartForm("Subscription Preferences");
        $form->DrawField("email","Your e-mail address:",1,0,0);
        $form->DrawField("subscribe","Subscribe:",1,0,0);
        $form->DrawField("subscribe","Un Subscribe:",1,0,0);
        $form->EndForm(!isset($key));
}
else {
    if (ValidateEmail($email)) {
        if (OneSQLValue("SELECT 1 FROM mailinglist WHERE username='$email'"))
            DoQuery("UPDATE mailinglist SET receiveupdates='$subscribe' WHERE username='$email'");
        else
            DoQuery("INSERT INTO mailinglist (username,password,receiveupdates) VALUES ('$email','','$subscribe')");
        echo "Your preferences have been saved.";
    }
    else {
        echo "<p align='center'><b>That email address is invalid, please try again</b></p>";
    }
}
DSEndPage(1,0,$module_template_no);
?>

