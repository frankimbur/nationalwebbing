<%
#######
# file .database.inc
# User-specific config file
# nationalwebbing
# August 02, 19104 at 09:42:48
#######

###########################################################################
# begin config

# server path to users files (no trailing slash)
$libpath            = "post";
$DSROOTPATH     = "/home/nationalwebbing.desktopmodules.com/public_html";
$DSSTAGINGPATH  = "/home/nationalwebbing.desktopmodules.com/public_html";

# database name
$db                 = "nationalwebbing";

# db values (we assume these are the same for both staging and live)
$dbuser             = "nationalwebbing";
$dbpassword         = "123nat456";
$dbhost             = "localhost";
if (strtolower($HTTP_HOST)=="localhost:7070")
    $dbhost             = "lserv1.dtopinc.com";
else
    $dbhost             = "localhost";

## Staging Information ##
# the host name of the staging server
# (if the URL of the staging server is staging.shockabsorbers.com, you would enter "staging")
$host_staging_name  = "staging";

# name that gets prepended to the db name
# (if the db name is "staging_test", you would enter "staging_")
$db_staging_name    = "";


# Debugging: 1 for On, 0 for Off
$DSDEBUG            = 0;

# these tables are excluded when the database is moved
# from staging to live or vice versa (has no effect on backup)
# these should not need to be changed
$exclude_tables     = array(
                            "active_sessions"
                            ,"auth_user"
                            ,"list"
                            ,"listdetail"
                            ,"log"
                            ,"nl_owner"
                            ,"nl_newsletter"
                            );

######
## module switches

# newsletter
#define ("NEWSLETTER", "true");
#define ("TODO", "true");
#define ("CALENDAR","true");
#define ("NEWS","true");
define ("SHIPPINGSTATUS","true");
define ("ORDERHISTORY","true");
#define ("RFQ","true");
#define ("SEARCH","true");
#define ("ZIPLOCATE","true");
#define ("CONTACTUS","true");
define ("POSTCARD","true");
#define ("REMINDER","true");
#define ("FAQ","true");
#define ("EMPLOYMENT","true");
#define ("TEMPLATE","true");


######
# end config
###########################################################################

# actual database name
$stagingdb          = $db_staging_name . $db;


// if user is editing the staging section, we must get the appropriate db
// don't edit this line
if (strlen($HTTP_HOST) && stristr($HTTP_HOST,$host_staging_name.".")) {
    $database=$stagingdb;
    $DSTHISPATH = $DSSTAGINGPATH;
}
else {
    $database=$db;
    $DSTHISPATH = $DSROOTPATH;
}

$DSHOST         = $dbhost;
$DSDATABASE     = $database;
$DSUSER         = $dbuser;
$DSPASSWORD     = $dbpassword;


if (class_exists("DB_Sql")) {
    class DB_Example extends DB_Sql {
        var $Host       = "";
        var $Database   = "";
        var $User       = "";
        var $Password   = "";

        // constructor will allow us to use variables in setting class data members
        function DB_Example()
        {
            global $DSDATABASE, $DSHOST,$DSUSER,$DSPASSWORD;

            $this->Host     = $DSHOST;
            $this->Database = $DSDATABASE;
            $this->User     = $DSUSER;
            $this->Password = $DSPASSWORD;
        }

    }
}

%>
